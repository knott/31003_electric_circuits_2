.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

	dot(,,1)
IND1:	inductor( down_ 1.5*elen_,W, ); llabel( ,L, )
	setrgb(dtudarkgreen)
	b_current( i_L(t) )
	resetrgb
	dot(,,1)

	setuncover(3,-)
	move to IND1.c + ( elen_,0 )
	"$\multimapdotbothA$" with .center at Here

	move to IND1.n+( 2*elen_, 0 )
	dot(,,1)
IND2:	inductor( down_ 0.75*elen_,W, ); rlabel( ,\uls L, )
VSRC:	source( down_ 0.75*elen_,V, ); rlabel( ,Li_L(0) \delta(t), )
	setrgb(dtudarkgreen)
	b_current( \underline{I}_L(\uls), below_, )
	resetrgb
	dot(,,1)
	resetuncover

	setuncover(5,-)
	move to IND2.s + ( 1.9*elen_,0 )
	"$\Rightarrow$" with .center at Here

	move to IND2.n+( 2.65*elen_, 0 )
	dot(,,1)
PAR1:	arrowline( down_ 0.375*elen_ ); setrgb(dtudarkgreen); rlabel( ,\underline{I}_L(\uls), ); resetrgb
	dot
	{
IND3:		inductor( down_ 0.75*elen_,W, ); rlabel( ,\uls L, )
		dot
	}
	line right_ 1.3*elen_
	corner
	source( down_ 0.75*elen_,I, ); rlabel( ,\frac{i_L(0) \delta(t)}{\uls}, )
	corner
	line to IND3.s
PAR2:	line down_ 0.375*elen_
	dot(,,1)
	resetuncover

	tikzcoordinate( IND1_n, IND1.n )
	tikzcoordinate( IND1_s, IND1.s )
	tikzcoordinate( IND2_n, IND2.n )
	tikzcoordinate( VSRC_s, VSRC.s )
	tikzcoordinate( PAR1_n, PAR1.n )
	tikzcoordinate( PAR2_s, PAR2.s )

.PE
