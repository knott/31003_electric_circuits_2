.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

OPAMP:	opamp( right_,,,1,R )

	move to OPAMP.In1
	line left_ 0.6*elen_
IN:	dot(,,1)
	move to Here+(0,-elen_)
	dot(,,1)
GND1:	ground()

	move to OPAMP.In2
	line left_ 0.3*elen_
	corner
	line down_ 0.3*elen_
	corner
	line to (OPAMP.Out.x,Here.y)
	corner
	line to OPAMP.Out
	dot
	line right_ 0.3*elen_
OUT:	dot(,,1)

	move to Here+(0,-elen_)
	dot(,,1)
GND2:	ground()

	tikzcoordinate( IN, IN )
	tikzcoordinate( GND1, GND1 )
	tikzcoordinate( OUT, OUT )
	tikzcoordinate( GND2, GND2 )

.PE
