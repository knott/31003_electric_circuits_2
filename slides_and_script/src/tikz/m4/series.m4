.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

Z1:	impedance( down_ elen_ ); llabel(,\underline{Z}_1,)
	setrgb(dtudarkgreen)
	b_current( \underline{I} )
	resetrgb
Z2:	impedance( down_ elen_ ); llabel(,\underline{Z}_2,)
	line down_ 0.5*elen_ dashed
ZN:	impedance( down_ elen_ ); llabel(,\underline{Z}_n,)

	tikzcoordinate( Z1_n, Z1.n )
	tikzcoordinate( Z1_s, Z1.s )
	tikzcoordinate( Z2_n, Z2.n )
	tikzcoordinate( Z2_s, Z2.s )
	tikzcoordinate( ZN_n, ZN.n )
	tikzcoordinate( ZN_s, ZN.s )

.PE
