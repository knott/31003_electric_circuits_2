.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

VS:	source( up_ elen_,V, ); llabel(,\underline{V}_T,)
#	setrgb(dtudarkgreen)
#	b_current(\underline{I}_{in},,O,E,0.2)
#	resetrgb
	corner
ZT:	impedance( right_ elen_ ); llabel(,\underline{Z}_T,)
	arrowline( right_ elen_ ); llabel(,\underline{I},)
OUT1:	dot
	line right_ 0.5*elen_
	corner
ZL:	impedance( down_ elen_ ); llabel(,\underline{Z}_L,)
	corner
	line left_ 0.5*elen_
OUT2:	dot
	line to VS.s
	corner

	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )

.PE
