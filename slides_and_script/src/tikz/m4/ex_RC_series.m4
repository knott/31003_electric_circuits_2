.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

VS:	source( up_ elen_ )
	corner
RES:	resistor( right_ 1.5*elen_,,E, ); rlabel( ,\SI{1000}{\ohm}, )
	setrgb(dtudarkgreen)
	b_current( i(t) )
	resetrgb
	corner
CAP:	capacitor( down_ elen_ ); rlabel( ,\SI{1}{\micro\farad} )
	corner
	line to VS.s
	corner

	tikzcoordinate( VS_n, VS.n )
	tikzcoordinate( VS_s, VS.s )
	tikzcoordinate( RES_w, RES.w )
	tikzcoordinate( RES_e, RES.e )
	tikzcoordinate( CAP_n, CAP.n )
	tikzcoordinate( CAP_s, CAP.s )

.PE
