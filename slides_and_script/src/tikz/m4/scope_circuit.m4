.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

IN1:	dot(,,1)
	line right_ 0.6*elen_
	dot
	{
RP:		resistor( right_ elen_,,E, ); llabel( ,R_{probe}, )
		dot
	}
	line down_ 0.3*elen_
	corner
CP:	capacitor( right_ elen_ ); rlabel( ,C_{probe}, )
	corner
	line up_ 0.3*elen_
	line right_ elen_
	dot
	{
R2:		resistor( down_ elen_,,E,); llabel( ,R_{scope}, )
		dot
	}
	line right_ 0.8*elen_
	dot
	{
		capacitor( down_ elen_ ); llabel( ,C_{scope}, )
		dot
		{
			line right_ 0.8*elen_
OUT2:			dot(,,1)
		}
		line to (IN1.x,Here.y)
IN2:		dot(,,1)
	}
	line to (OUT2.x,Here.y)
OUT1:	dot(,,1)

	tikzcoordinate( IN1, IN1 )
	tikzcoordinate( IN2, IN2 )
	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )

.PE
