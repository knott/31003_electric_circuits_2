.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

SRC:	source( up_ elen_ )
	corner
C1:	capacitor( right_ elen_ ); llabel( , C_1 )
	dot
	{
R1:		resistor( down_ elen_,,E, ); llabel( ,R_1, )
		dot
	}
	line right_ 0.3*elen_
	dot
	"A" above
C2:	capacitor( right_ elen_ ); llabel( , C_2 )
	dot
	{
R2:		resistor( down_ elen_,,E, ); llabel( ,R_2, )
		dot
		{
			line right_ 0.5*elen_
OUT2:			dot(,,1)
		}
		line to SRC.s
		corner
	}
	line right_ 0.5*elen_
OUT1:	dot(,,1)
	
	tikzcoordinate( SRC_n, SRC.n )
	tikzcoordinate( SRC_s, SRC.s )
	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )

.PE
