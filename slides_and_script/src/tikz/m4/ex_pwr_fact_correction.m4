.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

	dot(,,1); "b" ljust
	line left_ 1.5*elen_
	corner
	source( up_ elen_, AC, ); llabel( ,\SI{100}{\volt}\;\SI{0}{\degree})
	corner
	impedance( right_ 1.5*elen_ ); llabel(,\SI{10}{\ohm}+j\SI{5}{\ohm},)
	dot(,,1); "a" ljust

#	tikzcoordinate( VS_n, VS.n )
#	tikzcoordinate( VS_s, VS.s )

.PE
