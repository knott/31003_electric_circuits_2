.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

VS:	source( up_ elen_, I, )
	setrgb(dtudarkgreen)
	b_current(i_s,,O,E,0.2)
	resetrgb
	corner
	line right_ elen_
	dot
	{
RES:		resistor( down_ elen_,,E, ); llabel(,\SI{1}{\kilo\ohm},)
		setrgb(dtudarkgreen)
		b_current( i_R )
		resetrgb
		dot
	}
	line right_ elen_
	dot
	{
IND:		inductor( down_ elen_,W, ); llabel(,\SI{20}{\milli\henry},)
		setrgb(dtudarkgreen)
		b_current( i_L )
		resetrgb
		dot
	}
	line right_ elen_
	corner
CAP:	capacitor( down_ elen_ ); llabel(,\SI{500}{\nano\farad},)
	setrgb(dtudarkgreen)
	b_current( i_C )
	resetrgb
	corner
	line to VS.s
	corner

	tikzcoordinate( VS_n, VS.n )
	tikzcoordinate( VS_s, VS.s )

.PE
