.PS
cct_init
log_init

### defining additional components:
divert(-1)
	`pvcell( linespec, wid, ht )'
define(`pvcell',`eleminit_(`$1')
  define(`m4wd',ifelse(`$2',,`dimen_/2',`($2)'))dnl 
  define(`m4ht',ifelse(`$3',,`dimen_/5',`($3)'))dnl
  { line to rvec_(max(0,rp_len/2-m4wd/2),0)
    {[lbox(m4wd,m4ht)] at rvec_(m4wd/2,0)}
    {line from rvec_(0,-m4ht/2) to rvec_(m4wd/3,0) then to rvec_(0,m4ht/2)}
    move to rvec_(m4wd,0); line to rvec_(max(0,rp_len/2-m4wd/2),0) }
  line invis to rvec_(rp_len,0)')

    `impedance( linespec )'
define(`impedance',`eleminit_(`$1')
  define(`m4ht',`dimen_/5')dnl
  define(`m4wd',`m4ht*2')dnl
  {line to rvec_(max(0,rp_len/2-m4wd/2),0)
  	{[lbox(m4wd,m4ht)] at rvec_(m4wd/2,0)}
	{arc cw from rvec_(m4wd/4,0) to rvec_(m4wd/2,0) rad m4wd/7}
	{arc ccw from rvec_(m4wd/2,0) to rvec_(m4wd*3/4,0) rad m4wd/7}
	move to rvec_(m4wd,0); line to rvec_(max(0,rp_len/2-m4wd/2),0)
  }
  {[box invis ht_ m4ht wid_ m4wd] at rvec_(rp_len/2,0)}
  line to rvec_(rp_len,0) invis
')

## from Dwight Aplevich on 202002-11:
# `pvcell( linespec, wid, ht )'
#define(`pvcell',`eleminit_(`$1')
#  define(`m4ht',ifelse(`$3',,`dimen_/5',`($3)'))dnl
#  define(`m4wd',ifelse(`$2',,`m4ht*2',`($2)'))dnl
#  { line to rvec_(max(0,rp_len/2-m4wd/2),0)DADA
#    {[lbox(m4wd,m4ht)] at rvec_(m4wd/2,0)}
#    {line from rvec_(0,-m4ht/2) to rvec_(m4wd/3,0) then to rvec_(0,m4ht/2)}
#    move to rvec_(m4wd,0); line to rvec_(max(0,rp_len/2-m4wd/2),0) }
#  line invis to rvec_(rp_len,0)')

right_
divert(0)dnl

##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
dnl	`tikzcoordinate(name,location)'
define(`tikzcoordinate', `command sprintf("\draw (%g,%g) node [shape=coordinate] (`$1'){};", (`$2').x/scale, (`$2').y/scale)')
scale = 2.54		       # cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
arrowht = 0.25
#dimen_ = 3
### define DTU colors ###
# primary colors #
define( `dtured', `0.6, 0, 0' )
define( `dtugrey', `0.6 ,0.6, 0.6' )
# secondary colors #
define( `dtuyellow', `1.00, 0.80, 0.00' )
define( `dtuorange', `1.00, 0.60, 0.00' )
define( `dtulightred', `1.00, 0.00, 0.00' )
define( `dtubrown', `0.60, 0.00, 0.00' )
define( `dtupurple', `0.80, 0.20, 0.60' )
define( `dtuviolet', `0.40, 0.00, 0.60' )
define( `dtudarkblue', `0.20, 0.40, 0.80' )
define( `dtulightblue', `0.20, 0.80, 1.00' )
define( `dtulightgreen', `0.60, 0.80, 0.20' )
define( `dtudarkgreen', `0.40, 0.80, 0.00' )
define( `dtucoolgrey', `0.59, 0.58, 0.57' )
Origin: Here                   # Position names are capitalized

Z:	impedance( right_ elen_ ); llabel(,\underline{Z},)
#	setrgb(dtudarkgreen)
#	b_current(i_Z(t),,O,E,0.2)
#	resetrgb

	tikzcoordinate( Z_w, Z.w )
	tikzcoordinate( Z_c, Z.c )
	tikzcoordinate( Z_e, Z.e )

.PE
