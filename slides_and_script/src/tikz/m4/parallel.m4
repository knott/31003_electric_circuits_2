.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

IN1:	dot(,,1)
#	{
#		line right_ elen_#
#	}
#	resistor( right_ elen_ invisible )
	arrowline( right_ elen_)
	setrgb(dtudarkgreen)
	llabel(,\underline{I},)
#	b_current(\underline{I},,,,0.5)
	resetrgb
	{
		move to IN1 + (0,-1.5*elen_)
IN2:		dot(,,1)
	}
	dot
	{
Z1:		impedance( down_ 1.5*elen_ ); llabel(,\underline{Z}_1,)
		setrgb(dtudarkgreen)
		b_current( \underline{I_1} )
		resetrgb
		dot
	}
	line right_ elen_
	dot
	{
Z2:		impedance( down_ 1.5*elen_ ); llabel(,\underline{Z}_2,)
		setrgb(dtudarkgreen)
		b_current( \underline{I_2} )
		resetrgb
		dot
	}
	line right_ 0.25 * elen_
	line right_ 0.5* elen_ dotted
	line right_ 0.25 * elen_
	corner
ZN:	impedance( down_ 1.5*elen_ ); llabel(,\underline{Z}_n,)
	setrgb(dtudarkgreen)
	b_current( \underline{I_n} )
	resetrgb
	corner
	line left_ 0.25 * elen_
	line left_ 0.5* elen_ dotted
	line to IN2

	tikzcoordinate( IN1, IN1 )
	tikzcoordinate( IN2, IN2 )

.PE
