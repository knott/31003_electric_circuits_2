.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

RES:	resistor( up_ elen_,,E, ); llabel(,\SI{100}{\ohm},)
	corner
	move to RES.s
	corner
	line right_ elen_
	dot
	{
ISRC:		source( up_ elen_,I, ); llabel(,\SI{2}{\ampere}\;\SI{0}{\degree},)
		dot
		{
			line to RES.n
		}
IND:		inductor( right_ elen_,W, ); llabel(,j\SI{50}{\ohm},)
		dot(,,1)
	}
	line right_ elen_
	dot(,,1)

#	tikzcoordinate( VS_n, VS.n )
#	tikzcoordinate( VS_s, VS.s )

.PE
