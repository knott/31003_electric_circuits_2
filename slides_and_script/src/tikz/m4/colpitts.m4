.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

IN:	line right_ 0.5*elen_
	dot
	{
		line down_ 0.35*elen_
		corner
		line right_ 0.3*elen_
M2:		e_fet( up_ 0.75*elen_ ) with .G at Here; rlabel(,M_2,)

		move to M2.S
		dot(,,1)
		"$V_{ss}$" below
	}
	line up_ 0.35*elen_
	corner
	line right_ 0.3*elen_
M1:	e_fet( up_ 0.75*elen_,,P, ) with .G at Here; rlabel(,M_1,)

	move to M1.D
	dot(,,1)
	"$V_{dd}$" above

	line from M2.D to M1.S

	move to (M2.D.x,(M2.D.y+M1.S.y)/2)
	dot
	line right_ 0.7*elen_
	corner
	line down_ 1.2*elen_
	dot
	{
C2:		capacitor( down_ 0.5*elen_ )
		ground()
	}
	line left_ 0.5*elen_
CRYSTL:	[ linewid = linewid*2; xtal( left_ 0.5*elen_ ) ]
# with .e at Here
	move to CRYSTL.w
	line to (IN.w.x, Here.y)
	dot
	{
C1:		capacitor( down_ 0.5*elen_ )
		ground()
	}
	line to IN.w
	corner

.PE
