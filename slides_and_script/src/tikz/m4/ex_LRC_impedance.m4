.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

IN1:	dot(,,1)
IND:	inductor( right_ 1.5*elen_,W,); llabel(,\SI{100}{\milli\henry})
	corner
RES:	resistor( down_ elen_,,E, ); llabel( ,\SI{50}{\ohm}, )
	corner
CAP:	capacitor( left_ 1.5*elen_ ); llabel( ,\SI{10}{\micro\farad} )
	line to (IN1.x,Here.y)
IN2:	dot(,,1)

	tikzcoordinate( IN1, IN1 )
	tikzcoordinate( IN2, IN2 )

.PE
