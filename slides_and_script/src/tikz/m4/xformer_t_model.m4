.PS
cct_init
log_init

### defining additional components:
divert(-1)
    `pvcell( linespec, wid, ht )'
define(`pvcell',`eleminit_(`$1')
  define(`m4ht',ifelse(`$3',,`dimen_/5',`($3)'))dnl
  define(`m4wd',ifelse(`$2',,`m4ht*2',`($2)'))dnl
  {line to rvec_(max(0,rp_len/2-m4wd/2),0)
  	{lbox(m4wd,m4ht)}
	{line from rvec_(0,-m4ht/2) to rvec_(m4wd/3,0)}
	{line from rvec_(0,m4ht/2) to rvec_(m4wd/3,0)}
	move to rvec_(m4wd,0); line to rvec_(max(0,rp_len/2-m4wd/2),0)
  }
  {[box invis ht_ m4ht wid_ m4wd] at rvec_(rp_len/2,0)}
  line to rvec_(rp_len,0) invis
')
right_
divert(0)dnl

#include(build/tikz/m4/tikz.m4)
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
dnl	`tikzcoordinate(name,location)'
define(`tikzcoordinate', `command sprintf("\draw (%g,%g) node [shape=coordinate] (`$1'){};", (`$2').x/scale, (`$2').y/scale)')

#include(build/tikz/m4/beamer.m4)
##### add uncover for animating slides with beamer
## usage:
## setuncover(start,end)
## <whatever code should be animated>
## resetuncover
##
## where `start' is first slide to show and `end' is last slide (if all, use `-')
dnl   `setuncover(start,end)'
define(`setuncover', `command sprintf("\uncover<$1$2>{")')
dnl   `resetuncover()'
define(`resetuncover', `command sprintf("}")')

#include(build/tikz/m4/preamble.m4)
scale = 2.54 # everything in cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
arrowht = 0.25
#dimen_ = 3
### define DTU colors ###
# primary colors #
define( `dtured', `0.6, 0, 0' )
define( `dtugrey', `0.6 ,0.6, 0.6' )
# secondary colors #
define( `dtuyellow', `1.00, 0.80, 0.00' )
define( `dtuorange', `1.00, 0.60, 0.00' )
define( `dtulightred', `1.00, 0.00, 0.00' )
define( `dtubrown', `0.60, 0.00, 0.00' )
define( `dtupurple', `0.80, 0.20, 0.60' )
define( `dtuviolet', `0.40, 0.00, 0.60' )
define( `dtudarkblue', `0.20, 0.40, 0.80' )
define( `dtulightblue', `0.20, 0.80, 1.00' )
define( `dtulightgreen', `0.60, 0.80, 0.20' )
define( `dtudarkgreen', `0.40, 0.80, 0.00' )
define( `dtucoolgrey', `0.59, 0.58, 0.57' )

Origin:	Here

IN1:	dot(,,1)
L1:	inductor( right_ 1.5*elen_, W); llabel( , L_1-M )
	b_current(i_1( t ),below_)
	dot
	{
LMUT:		inductor( down_ elen_ ,W, ); rlabel(, M,)
		dot
	}
L2:	inductor( right_ 1.5*elen_, W); llabel( , L_2-M )
	b_current(i_2( t ),below_,,E)
OUT1:	dot(,,1)

	move to OUT1+(0,-elen_)
OUT2:	dot(,,1)
	line to (IN1.x,Here.y)
IN2:	dot(,,1)

	tikzcoordinate( IN1, IN1 )
	tikzcoordinate( IN2, IN2 )
	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )

.PE
