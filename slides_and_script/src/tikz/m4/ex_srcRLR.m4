.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

SRC:	source( up_ elen_ )
	corner
R1:	resistor( right_ elen_,,E, ); llabel( ,\SI{100}{\ohm}, )
IND:	inductor( right_ elen_,W,); llabel( ,\SI{100}{\milli\henry}, )
	dot
	{
R2:		resistor( down_ elen_,,E, ); llabel( ,\SI{25}{\ohm}, )
		dot
		{
			line right_ 0.5*elen_
OUT2:			dot(,,1)
		}
		line to SRC.s
		corner
	}
	line right_ 0.5*elen_
OUT1:	dot(,,1)

	tikzcoordinate( SRC_n, SRC.n )
	tikzcoordinate( SRC_s, SRC.s )
	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )

.PE
