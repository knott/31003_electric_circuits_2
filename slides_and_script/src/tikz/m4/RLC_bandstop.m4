.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

SRC:	source( up_ elen_ )
	corner
RES:	resistor( right_ elen_,,E, ); llabel( ,R, )
	corner
IND:	inductor( down_ 0.5*elen_,W, ); rlabel( ,L, )
CAP:	capacitor( down_ 0.5*elen_ ); rlabel( ,C, )
	corner
	line to SRC.s
	corner

	tikzcoordinate( SRC_n, SRC.n )
	tikzcoordinate( SRC_s, SRC.s )
	tikzcoordinate( IND_n, IND.n )
	tikzcoordinate( CAP_s, CAP.s )


.PE
