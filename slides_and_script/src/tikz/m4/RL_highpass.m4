.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

SRC:	source( up_ elen_ )
	corner
RES:	resistor( right_ elen_,,E, ); llabel( ,R, )
	corner
IND:	inductor( down_ elen_,W, ); rlabel( ,L, )
	corner
	line to SRC.s
	corner

	tikzcoordinate( SRC_n, SRC.n )
	tikzcoordinate( SRC_s, SRC.s )
	tikzcoordinate( IND_n, IND.n )
	tikzcoordinate( IND_s, IND.s )


.PE
