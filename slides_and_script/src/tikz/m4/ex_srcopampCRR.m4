.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

	ground()
SRC:	source( up_ 1.5*elen_ ); llabel(, \underline{V}_1(\uls), )
	corner
	line right_ elen_
OPAMP:	opamp( right_,{\scriptsize$\;\;\;\;+$},{\scriptsize$\;\;\;\;-$},1, ) with .In1 at Here
#OPAMP:	opamp( right_,\;\;\;+,\;\;\;-,1, ) with .In1 at Here

	move to OPAMP.In2
	line left_ 0.2*elen_
	corner
	line down_ 0.3*elen_
	dot
	{
		{
R1:			resistor( down_ to (Here.x,SRC.s.y),,E, ); llabel(,2R,)
			ground()
		}
		line left_ 0.4*elen_
		corner
CAP:		capacitor( down_ to (Here.x,SRC.s.y) ); rlabel(,C,)
		corner
		line to R1.s
		dot
	}
R2:	resistor( right_ to (OPAMP.Out.x,Here.y),,E, ); rlabel(,2R,)
	corner
	line to OPAMP.Out
	dot
	line right_ 0.3*elen_
OUT1:	dot(,,1)

	move to ( OUT1.x,R1.s.y )
OUT2:	dot(,,1)
	ground()

	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )

.PE
