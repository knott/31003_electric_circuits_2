divert(-1)
  preamble.m4

define(`preamble_')

scale = 2.54 # everything in cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
arrowht = 0.25
#dimen_ = 3
### define DTU colors ###
# primary colors #
define( `dtured', `0.6, 0, 0' )
define( `dtugrey', `0.6 ,0.6, 0.6' )
# secondary colors #
define( `dtuyellow', `1.00, 0.80, 0.00' )
define( `dtuorange', `1.00, 0.60, 0.00' )
define( `dtulightred', `1.00, 0.00, 0.00' )
define( `dtubrown', `0.60, 0.00, 0.00' )
define( `dtupurple', `0.80, 0.20, 0.60' )
define( `dtuviolet', `0.40, 0.00, 0.60' )
define( `dtudarkblue', `0.20, 0.40, 0.80' )
define( `dtulightblue', `0.20, 0.80, 1.00' )
define( `dtulightgreen', `0.60, 0.80, 0.20' )
define( `dtudarkgreen', `0.40, 0.80, 0.00' )
define( `dtucoolgrey', `0.59, 0.58, 0.57' )

	`pvcell( linespec )'
define(`pvcell',
`	eleminit_(`$1')
	{
		line to rvec_(max(0,rp_len/2-m4ht/2),0)
		{
			lbox(m4wd,m4ht)
		}
		{
			line from rvec_(0,-m4ht/2) to rvec_(m4wd/3,0)
		}
		{
			line from rvec_(0,m4ht/2) to rvec_(m4wd/3,0)
		}
		move to rvec_(1.5*m4wd,0); line to rvec_(max(0,rp_len/2-m4ht/2),0)
	}
	{[box invis ht_ m4ht wid_ m4wd] at rvec_(rp_len/2,0)}
	line to rvec_(rp_len,0) invis
')
#	line invis from 2nd last line.start to 2nd last line.end


#`pvcell( linespec, cycle wid )
#	 cycle wid: default is dimen_/6'
#define(`pvcell',
#`define(`m4h',`ifelse(`$2',,`dimen_/6',`($4)')/4')dnl
# `ebox(`$1',shift(shift($@)))',
# {
#	line from last line.start+vec_(m4wd*0.3,m4ht/2)
#	line from 2nd last line.start+vec_(m4wd*0.3,-m4ht/2)
# }
#')

right_
divert(0)dnl
