.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

# DIFFERENTIAL INPUT STAGE:
NINV:	dot(,,1)
	line right_ 0.5*elen_
	dot
	{
		line down_ 0.2*elen_
DIF:		dot(,,1)
	}
	line right_ 0.2*elen_
M1:	e_fet( up_ elen_ ) with .G at Here
	move to M1.D
#	line up_ 0.2*elen_
RD1:	resistor( up_ 0.7*elen_,,E, ); llabel(,R_d,)
	dot
	line up_ 0.2*elen_
VDD:	dot(,,1)
	"$V_{dd}$" above

	move to NINV+( DIF.x, -0.7*elen_ )
INV:	dot
	line right_ elen_
M2:	e_fet( up_ elen_ ) with .G at Here
	move to M2.D
	line to Here.x,RD1.s.y
	{
RD2:		resistor( to (Here.x,RD1.n.y),,E, ); llabel(,R_d,)
		corner
		line to RD1.n
	}

	move to M1.S
	line to (Here.x,M2.S.y)
	corner
	line to ((M1.S.x+M2.S.x)/2,Here.y)
	dot
	{
ISRC1:		source( down_ 0.5*elen_, I, )
		dot
		{
			line down_ 0.2*elen_
VSS:			dot(,,1)
			"$V_{ss}$" below
		}
		line to (INV.x,Here.y)
		dot
		{
			line up_ 0.2*elen_
GND2:			dot(,,1)
		}
		line to (NINV.x,Here.y)
GND1:		dot(,,1)
	}
	line to (M2.S.x,Here.y)
	corner
	line to M2.S

# VOLTAGE AMPLIFICATION STAGE:
	setuncover(2,-)

	move to RD1.s
	dot
	line right_ 1.4*elen_
M3:	e_fet( up_ elen_ ) with .G at Here

	move to M3.D
	line to (Here.x,RD1.n.y)
	corner
	line to RD2.n
	dot

	move to M3.S
	line to (Here.x,ISRC1.n.y)
ISRC2:	source( to (Here.x,ISRC1.s.y ),I,)
	corner
	line to ISRC1.s
	dot
	resetuncover

# CURRENT AMPLIFICATION STAGE
	setuncover(3,-)
	move to (M3.S.x,M2.D.y)
	dot
	line right_ 0.5*elen_
	dot
	{
		line up_ 0.3*elen_
		corner
		line right_ 0.3*elen_
M4:		e_fet( up_ elen_ ) with .G at Here

		move to M4.D
		line to (Here.x,RD1.n.y)
		corner
		line to (M3.D.x,Here.y)
		dot
	}
	line down_ 0.4*elen_
	corner
	line right_ 0.3*elen_
M5:	e_fet( up_ elen_ ) with .G at Here

	move to M5.D
	line to (Here.x,(M5.D.y+M4.S.y)/2)
	{
		dot
		line right 0.5*elen_
OUT:		dot(,,1)
	}
	line to M4.S

	move to M5.S
	line to (Here.x,ISRC1.s.y)
	dot
	{
		line to (OUT.x,Here.y)
GND3:		dot(,,1)
	}
	line to ISRC2.s
	dot
	resetuncover

	setuncover(4,-)
	setrgb(dtuorange)
	move to M3.G+(-0.2*elen_,0)
	dot
	line up_ 0.5*elen_
	corner
CAP:	capacitor( to (M3.D.x,Here.y) )
	dot
	resetrgb
	resetuncover

	tikzcoordinate( NINV, NINV )
	tikzcoordinate( INV, INV )
	tikzcoordinate( DIF, DIF )
	tikzcoordinate( GND1, GND1 )
	tikzcoordinate( GND2, GND2 )
	tikzcoordinate( OUT, OUT )
	tikzcoordinate( GND3, GND3 )

.PE
