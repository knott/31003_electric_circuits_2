.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

SRC:	source( up_ elen_ )
	corner
	resistor( right_ elen_,,E, ); llabel( ,R, )
	corner
CAP:	capacitor( down_ elen_ ); rlabel( ,C, )
	corner
	line to SRC.s
	corner

	tikzcoordinate( SRC_n, SRC.n )
	tikzcoordinate( SRC_s, SRC.s )
	tikzcoordinate( CAP_n, CAP.n )
	tikzcoordinate( CAP_s, CAP.s )


.PE
