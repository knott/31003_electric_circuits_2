.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

IN1:	dot(,,1)
R1:	resistor( right_ elen_,,E, ); llabel( ,\SI{5}{\kilo\ohm}, )
	dot
	{
		line down_ 0.3*elen_
		corner
		line right_ 0.2*elen_
OPAMP:		opamp( right_,,,0.8, ) with .In1 at Here
	}
	{
R2:		resistor( right_ to (OPAMP.Out.x,Here.y),,E,); llabel( ,\SI{100}{\kilo\ohm}, )
		dot
		line right_ 0.3*elen_
OUT1:		dot(,,1)	
	}
	line up_ 0.5*elen_
	corner
CAP:	capacitor( right_ to (OPAMP.Out.x,Here.y) ); llabel( ,\SI{0.02}{\micro\farad}, )
	corner
	line to OPAMP.Out
	corner

	move to OPAMP.In2
	line left_ 0.2*elen_
	corner
	line down_ 0.3*elen_
	dot
	{
		line to (OUT1.x,Here.y)
OUT2:		dot(,,1)
	}
	line to (IN1.x,Here.y)
IN2:	dot(,,1)

	tikzcoordinate( IN1, IN1 )
	tikzcoordinate( IN2, IN2 )
	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )

.PE
