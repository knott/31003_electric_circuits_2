.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

OPAMP:	opamp( elen_ )
	move to OPAMP.E1
	line up_ 0.4*elen_; "$V_{dd}$" above
	dot( , , 1 )
	move to OPAMP.E2
	line down_ 0.4*elen_; "$V_{ss}$" below
	dot( , , 1 )
	
	move to OPAMP.In1
INN:	line left_ elen
	move to OPAMP.In2
INP:	line left_ 0.5*elen
	move to OPAMP.Out
	line right_ 0.1*elen_
	corner
	line up_ elen_
	corner
	line to (INP.w.x,Here.y)
	dot
	{
RH1:		resistor( down_ elen_, E, )
		line to INP.w
		dot
RH2:		resistor( down_ elen_, E, )
GND:		ground
	}
	line to (INN.w.x,Here.y)
	corner
RES:	resistor( down_ elen_, E, )
	line to INN.w
	dot
	{
TRI:		arrow left_ elen_; "triangle" above
	}
	capacitor( to (Here.x,GND.y) )
	ground

.PE
