.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

IN1:	dot(,,1)
	line right_ 0.25*elen_
	corner
CAP1:	capacitor( down_ 0.75*elen_ ); llabel( , 2C, )
	dot
	{
RES1:		resistor( down_ 1.5*elen_,,E, ); llabel( , 2R, )
		dot
	}
	line right_ 0.75*elen_
	corner
RES2:	resistor( down_ 0.75*elen_,,E, ); llabel( , 2R, )
CAP2:	capacitor( down_ 0.75*elen_ ); llabel( , C, )
	corner
	line to (IN1.x,Here.y)
IN2:	dot(,,1)

	tikzcoordinate( IN1, IN1 )
	tikzcoordinate( IN2, IN2 )

.PE
