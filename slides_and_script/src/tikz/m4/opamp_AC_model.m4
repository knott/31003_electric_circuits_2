.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

NINV:	dot(,,1); "$V_p$" above_
	line right_ elen_

	move to NINV + (0,-elen_)
INV:	dot(,,1); "$V_n$" above_
	line right_ elen_

	move right_ elen_
	{
SRC:		consource( up_ elen_, V); rlabel(,\frac{A}{1 + \frac{\underline{s}}{\omega_c} },)
		corner
		line right_ elen_
OUT1:		dot(,,1)
	}
	corner
	line right_ elen_
OUT2:	dot(,,1)

	tikzcoordinate( NINV, NINV )
	tikzcoordinate( INV, INV )
	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )

.PE
