.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

GAIN:	amp( right_ ); llabel(,A,)
	line right_ 0.5*elen_
	corner
	line down_ 0.5*elen_

	dot
	{
RES:		resistor( down_ 0.75*elen_,,E,); llabel(,R,)
	}
	line left_ elen_
	dot
	{
CAP:		capacitor( down_ 0.75*elen_ ); llabel(,C,)
		dot
		ground()
	}
	line left_ elen_
	dot
	{
IND:		inductor( down_ 0.75*elen_, W,); llabel(,L,)
		corner
		line to RES.s
		corner
	}
	line to (Here.x,GAIN.w.y) \
	  then to (GAIN.w)

.PE
