.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

IND:	inductor( up_ elen_,W, ); llabel( ,L, )
	corner
	line right_ 0.75*elen_
	dot
	{
CAP:		capacitor( down_ elen_ ); llabel( ,C, )
		dot
	}
	line right_ 0.75*elen_
	dot
	{
R1:		resistor( down_ elen_,,E, ); llabel( ,R, )
		dot
		{
			line right_ 0.75*elen_
OUT2:			dot(,,1)
		}
		line to IND.s
		corner
	}
	{
		line right_ 0.75*elen_
OUT1:		dot(,,1)
	}

	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )

.PE
