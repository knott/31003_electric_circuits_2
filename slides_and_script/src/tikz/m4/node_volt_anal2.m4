.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

SRC1:	source( up_ elen_,I, ); llabel( ,\underline{I}_a = \frac{ \underline{V}_a }{ \underline{Z}_1 } )
	corner
#	b_current( \underline{I}_a = \frac{ \underline{V}_a }{ \underline{Z}_1 } )
	line right_ 0.75*elen_
	dot
	{
Z1:		impedance( down_ elen_ ); rlabel( ,\underline{Z}_1, )
		dot
	}
	line right_ 0.75*elen_; "A" above
	dot
	{
Z2:		impedance( down_ elen_ ); rlabel( ,\underline{Z}_2, )
		dot
	}
Z3:	impedance( right_ 2.25*elen_ ); llabel( ,\underline{Z}_3, )
	dot
	{
Z4:		impedance( down_ elen_ ); llabel( ,\underline{Z}_4, )
		dot
	}
	line right_ 0.75*elen_; "B" above
	dot
	{
Z5:		impedance( down_ elen_ ); llabel( ,\underline{Z}_5, )
		dot
	}
	line right_ 0.75*elen_
	corner
SRC2:	reversed( `source', down_ elen_,I, ); llabel( ,\underline{I}_b = \frac{ \underline{V}_b }{ \underline{Z}_5 }, )
	corner
#	b_current( \underline{I}_b = \frac{\underline{V}_b}{\underline{Z}_5} )
	line to (Z3.x,Here.y)
	{
		dot
		ground()
	}
	line to SRC1.s
	corner

	tikzcoordinate( Z2_n, Z2.n )
	tikzcoordinate( Z2_s, Z2.s )
	tikzcoordinate( Z4_n, Z4.n )
	tikzcoordinate( Z4_s, Z4.s )

.PE
