.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

	dot(,,1)
	line left_ elen_
	corner
SRC:	source( up_ elen_ ); llabel( ,\SI{100}{\volt}\;\SI{0}{\degree}, )
	corner
IMP:	impedance( right_ elen_ ); llabel(,\SI{10}{\ohm}+j\SI{5}{\ohm},)
	dot(,,1)

#	tikzcoordinate( VS_n, VS.n )
#	tikzcoordinate( VS_s, VS.s )

.PE
