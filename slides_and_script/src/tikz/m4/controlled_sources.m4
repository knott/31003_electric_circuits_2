.PS
cct_init
log_init

#include(build/tikz/m4/tikz.m4)
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
dnl	`tikzcoordinate(name,location)'
define(`tikzcoordinate', `command sprintf("\draw (%g,%g) node [shape=coordinate] (`$1'){};", (`$2').x/scale, (`$2').y/scale)')

#include(build/tikz/m4/beamer.m4)
##### add uncover for animating slides with beamer
## usage:
## setuncover(start,end)
## <whatever code should be animated>
## resetuncover
##
## where `start' is first slide to show and `end' is last slide (if all, use `-')
dnl   `setuncover(start,end)'
define(`setuncover', `command sprintf("\uncover<$1$2>{")')
dnl   `resetuncover()'
define(`resetuncover', `command sprintf("}")')

#include(build/tikz/m4/preamble.m4)
scale = 2.54 # everything in cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
arrowht = 0.25
#dimen_ = 3
### define DTU colors ###
# primary colors #
define( `dtured', `0.6, 0, 0' )
define( `dtugrey', `0.6 ,0.6, 0.6' )
# secondary colors #
define( `dtuyellow', `1.00, 0.80, 0.00' )
define( `dtuorange', `1.00, 0.60, 0.00' )
define( `dtulightred', `1.00, 0.00, 0.00' )
define( `dtubrown', `0.60, 0.00, 0.00' )
define( `dtupurple', `0.80, 0.20, 0.60' )
define( `dtuviolet', `0.40, 0.00, 0.60' )
define( `dtudarkblue', `0.20, 0.40, 0.80' )
define( `dtulightblue', `0.20, 0.80, 1.00' )
define( `dtulightgreen', `0.60, 0.80, 0.20' )
define( `dtudarkgreen', `0.40, 0.80, 0.00' )
define( `dtucoolgrey', `0.59, 0.58, 0.57' )

#ifdef(`tikz_',,`include(build/tikz/m4/tikz.m4)divert(-1)')
#ifdef(`beamer_',,`include(build/tikz/m4/beamer.m4)divert(-1)')
#ifdef(`preamble_',,`include(build/tikz/m4/preamble.m4)divert(-1)')

Origin:	Here

CSRC:	consource( up_ elen_ )
	move to CSRC.s + ( 0.5*elen_, 0)
ARSRC:	variable( source( up_ elen_ ) )
	move to ARSRC.s + ( 0.5*elen_, 0)
CCSRC:	consource( up_ elen_, I, )
	move to CCSRC.s + ( 0.5*elen_, 0)
CCSRC2:	consource( up_ elen_, i, )
	move to CCSRC2.s + ( 0.5*elen_, 0)
ARCSRC:	variable( source( up_ elen_, I, ) )
	move to ARCSRC.s + ( 0.5*elen_, 0)
CVSRC:	consource( up_ elen_, V, )
	move to CVSRC.s + ( 0.5*elen_, 0)
CVSRC2:	consource( up_ elen_, i, )
	move to CVSRC2.s + ( 0.5*elen_, 0)
ARVSRC:	variable( source( up_ elen_, v, ) )
	move to ARVSRC.s + ( 0.5*elen_, 0)

	tikzcoordinate( CSRC_n, CSRC.n )
	tikzcoordinate( ARSRC_n, ARSRC.n )
	tikzcoordinate( CCSRC_n, CCSRC.n )
	tikzcoordinate( CCSRC2_n, CCSRC2.n )
	tikzcoordinate( ARCSRC_n, ARCSRC.n )
	tikzcoordinate( CVSRC_n, CVSRC.n )
	tikzcoordinate( CVSRC2_n, CVSRC2.n )
	tikzcoordinate( ARVSRC_n, ARVSRC.n )

.PE
