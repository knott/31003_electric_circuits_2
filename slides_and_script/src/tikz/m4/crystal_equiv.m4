.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

	line right_ 0.3*elen_
	dot
	{
		line down_ 0.4*elen_
		corner
IND:		inductor( right_ 0.75*elen_,W, ); llabel(,L,)
CAPS:		capacitor( right_ 0.75*elen_ ); llabel(,C_s,)
RES:		resistor( right_ 0.75*elen_,,E, ); llabel(,R,)
		corner
		line up_ 0.4*elen_
		dot
	}
	line up_ 0.4*elen_
	corner
CAPP:	capacitor( right_ to (RES.e.x,Here.y) ); llabel(,C_p,)
	corner
	line down_ 0.4*elen_
	line right_ 0.3*elen_


.PE
