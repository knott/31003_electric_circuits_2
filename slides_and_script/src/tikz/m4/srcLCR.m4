.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

SRC:	source( up_ elen_ )
	corner
	line right_ elen_
IN1:	dot
IND:	inductor( right_ elen_,W, ); llabel( ,L, )
	dot
	{
CAP:		capacitor( down_ elen_ ); llabel( ,C, )
		dot
	}
	line right_ 0.75*elen_
	dot
	{
R1:		resistor( down_ elen_,,E, ); llabel( ,R, )
		dot
		{
			line right_ 0.75*elen_
OUT2:			dot(,,1)
		}
		line left_ to (IN1.x,Here.y)
IN2:		dot
		line to SRC.s
		corner
	}
	{
		line right_ 0.75*elen_
OUT1:		dot(,,1)
	}


	tikzcoordinate( SRC_n, SRC.n )
	tikzcoordinate( SRC_s, SRC.s )
	tikzcoordinate( IN1, IN1 )
	tikzcoordinate( IN2, IN2 )
	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )

.PE
