.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

SRC:	source( up_ 2*elen_, V, )#; llabel( , V_{in} )
	corner
	line right_ 1.5*elen_
	corner
PRIM:	inductor( down_ elen_, L, ); llabel( N_1 )
	{
		setonly(1,)
		reversed( `switch', down_ elen_ )
		resetonly
	}
	{
		setonly(2,)
		reversed( `lswitch', down_ elen_, , C )
		resetonly
	}
	setonly(3,)
	reversed( `lswitch', down_ elen_, , O )
	resetonly
	corner
SW:	line to SRC.s
	corner

	move to PRIM.s + ( 0.9*elen_, 0 )
SEC:	inductor( up_ elen_, L, ); llabel( , , N_2 )
	corner
	diode( right_ elen_ )
	dot
	{
CAP:		capacitor( down_ elen_ )
		dot
	}
	line right_ elen_
	corner
LOAD:	resistor( down_ elen_, , E, )
	corner
	line to SEC.s

# gate drive:
      move to SW.s
      line left_ 0.3*elen_
      dot
VGS:  source( up_ 0.5*elen_, P, ); llabel( , V_{GS} )
      corner
      line right_ 0.2*elen_

# core and winding directions:
        move to PRIM.s + ( 0.4*elen_, 0.3*elen_ )
	line up_ 0.4*elen_
	move to PRIM.s + ( 0.5*elen_, 0.3*elen_ )
	line up_ 0.4*elen_
	move to PRIM.n + ( 0.1*elen_, -0.3*elen_ )
	dot
	move to SEC.s + ( -0.1*elen_, 0.3*elen_ )
	dot

	tikzcoordinate( SRC, SRC )
	tikzcoordinate( SRC_n, SRC.n )
	tikzcoordinate( SRC_s, SRC.s )
	tikzcoordinate( PRIM_n, PRIM.n )
	tikzcoordinate( PRIM_s, PRIM.s )
	tikzcoordinate( SW_s, SW.e )
	tikzcoordinate( SEC_n, SEC.n )
	tikzcoordinate( SEC_s, SEC.s )
	tikzcoordinate( CAP_n, CAP.n )
	tikzcoordinate( CAP_s, CAP.s )
	tikzcoordinate( LOAD_n, LOAD.n )
	tikzcoordinate( LOAD_s, LOAD.s )

.PE
