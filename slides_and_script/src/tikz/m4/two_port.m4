.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

#TWOPRT:	nport( wid 1.6*elen_ ht 0.9*elen_ "two-port",1,0,1,0,3,,N )
TWOPRT:	nport( wid 2.88*elen_ ht 1.62*elen_ "two-port",1,0,1,0,3,,N )

	move to TWOPRT.W1a
#	reversed( `arrowline', left_ 0.5*elen_ ); setrgb(dtudarkgreen); rlabel( ,\underline{I}_{in}(\uls), ); resetrgb
	reversed( `arrowline', left_ 1.4*elen_ ); setrgb(dtudarkgreen); rlabel( ,\underline{I}_{in}(\uls), ); resetrgb
	corner
VSRC:	source( down_ to (Here.x,TWOPRT.W1b.y) )
	corner
	line to TWOPRT.W1b

	move to TWOPRT.E1a
#	arrowline( right_ 0.5*elen_ ); setrgb(dtudarkgreen); llabel( ,\underline{I}_{out}(\uls), ); resetrgb
	arrowline( right_ 1.4*elen_ ); setrgb(dtudarkgreen); llabel( ,\underline{I}_{out}(\uls), ); resetrgb
	corner
ZLOAD:	impedance( down_ to (Here.x,TWOPRT.E1b.y) )
	corner
	line to TWOPRT.E1b

	tikzcoordinate( TWOPRT_w, TWOPRT.w )
	tikzcoordinate( TWOPRT_e, TWOPRT.e )
	tikzcoordinate( VSRC_n, VSRC.n )
	tikzcoordinate( VSRC_s, VSRC.s )
	tikzcoordinate( ZLOAD_n, ZLOAD.n )
	tikzcoordinate( ZLOAD_s, ZLOAD.s )

.PE
