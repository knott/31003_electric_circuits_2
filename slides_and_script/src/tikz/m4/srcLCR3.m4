.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

SRC:	source( up_ elen_ )
	corner
IND:	inductor( right_ elen_,W, ); llabel( ,L, )
SRCL:	source( right_ elen_, V,)
	dot
	{
RES:		resistor( down_ elen_,,E, ); llabel( ,R, )
		dot
	}
	line right_ 1.5*elen_
	corner
CAP:	capacitor( down_ 0.5*elen_ ); llabel( ,C, )
SRCC:	reversed( `source', down_ 0.5*elen_, V, )
	corner
	line to SRC.s
	corner

	tikzcoordinate( SRC_n, SRC.n )
	tikzcoordinate( SRC_c, SRC.c )
	tikzcoordinate( SRC_s, SRC.s )
	tikzcoordinate( SRCL_w, SRCL.w )
	tikzcoordinate( SRCL_e, SRCL.e )
	tikzcoordinate( RES_n, RES.n )
	tikzcoordinate( RES_c, RES.c )
	tikzcoordinate( RES_s, RES.s )
	tikzcoordinate( CAP_n, CAP.n )
	tikzcoordinate( CAP_s, CAP.s )
	tikzcoordinate( SRCC_n, SRCC.n )
	tikzcoordinate( SRCC_s, SRCC.s )

.PE
