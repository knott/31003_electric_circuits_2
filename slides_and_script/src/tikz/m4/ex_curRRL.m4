.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

SRC:	source( up_ elen_,I, )
	b_current(I_1(\uls),)
	corner
	line right_ 0.5*elen_
	dot
	{
R1:		resistor( down_ elen_,,E, ); llabel( ,R, )
		dot
	}
R2:	resistor( right_ elen_,,E, ); llabel( ,2R, )
	corner
IND:	inductor( down_ elen_,W, ); llabel( ,L, ); b_current(I_2(\uls),)
	corner
	line to SRC.s
	corner

#	tikzcoordinate( SRC_n, SRC.n )
#	tikzcoordinate( SRC_s, SRC.s )
#	tikzcoordinate( OUT1, OUT1 )
#	tikzcoordinate( OUT2, OUT2 )

.PE
