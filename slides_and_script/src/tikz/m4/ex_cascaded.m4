.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

	
IN1:	dot(,,1)
C1:	capacitor( right_ elen_ ); llabel( ,\SI{47}{\nano\farad}, )
	dot
	{
R1:		resistor( down_ 1.75*elen_,,E, ); rlabel( ,\SI{22}{\kilo\ohm}, )
		dot
	}
	line right_ 0.75*elen_
CON1:	dot
	line right_ 0.75*elen_
OPAMP:	opamp( right_,{\scriptsize$\;\;\;\;+$},{\scriptsize$\;\;\;\;-$},1, ) with .In1 at Here
#OPAMP:	opamp( right_,\;\;\;+,\;\;\;-,1, ) with .In1 at Here

	move to OPAMP.In2
	line left_ 0.2*elen_
	corner
	line down_ 0.3*elen_
	dot
	{
C2:		capacitor( down_ 0.5*elen_ ); rlabel(,\SI{100}{\nano\farad},)
R2:		resistor( down_ to (Here.x,R1.s.y),,E, ); rlabel(,\SI{1}{\kilo\ohm},)
		dot
	}
R3:	resistor( right_ to (OPAMP.Out.x,Here.y),,E, ); rlabel(,\SI{10}{\kilo\ohm},)
	corner
	line to OPAMP.Out
	dot
	line right_ 0.3*elen_
OUT1:	dot(,,1)

	move to (IN1.x,R2.s.y)
IN2:	dot(,,1)
	line right_ to (CON1.x,Here.y)
	dot
	line right_ to (OUT1.x,Here.y)
OUT2:	dot(,,1)

	tikzcoordinate( IN1, IN1 )
	tikzcoordinate( IN2, IN2 )
	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )

.PE
