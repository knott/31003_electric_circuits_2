.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

	ground()
SRC:	source( up_ elen_, P,)
	corner
	resistor( right_ elen_,,E, )
INV:	dot
	line right_ 0.3*elen_	
OPAMP:	opamp( elen_ ) with .In1 at Here
	
	move to OPAMP.In2
	line left_ 0.3*elen_
	corner
	ground()

	move to OPAMP.Out
	line right_ 0.1*elen
	dot
	{
		line up_ 0.75*elen_
		corner
		capacitor( left_ to (INV.x,Here.y) )
		corner
		line to INV
	}
TRI:	arrow right_ 0.5*elen_; "triangle" above

.PE
