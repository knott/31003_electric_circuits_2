.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

OPAMP:	opamp( right_,,,1, )
	move to OPAMP.In1
	line left_ 0.3*elen_
	move to OPAMP.In2
	line left_ 0.3*elen_

.PE
