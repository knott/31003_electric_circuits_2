.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

SRC:	source( up_ elen_ )
	dot
	{
C1:		capacitor( right_ elen_ ); llabel( , C_1 )
		dot
		{
R1:			resistor( down_ elen_,,E, ); llabel( ,R_1, )
			dot
		}
C2:		capacitor( right_ elen_ ); llabel( , C_2 )
		dot
		line right_ 0.5*elen_
OUT1:		dot(,,1)
	}
	line up_ 0.75*elen_
R2:	resistor( right_ 2*elen_,,E, ); llabel( ,R_2, )
	line to C2.e

	move to SRC.s
	corner
	line to (OUT1.x,Here.y)
OUT2:	dot(,,1)
	
	tikzcoordinate( SRC_n, SRC.n )
	tikzcoordinate( SRC_s, SRC.s )
	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )

.PE
