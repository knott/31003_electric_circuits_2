.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

	
IN1:	dot(,,1)
	line right_ elen_
NINV:	arrowline( right_ 0.5*elen_ ); llabel(,\underline{I}_P,)
OPAMP:	opamp( right_,{\scriptsize$\;\;\;\;+$},{\scriptsize$\;\;\;\;-$},1.2, ) with .In1 at Here

	move to OPAMP.In2
INV:	reversed( `arrowline', left_ 0.5*elen_ ); llabel(,\underline{I}_P,)
	corner
	line down_ 0.5*elen_
	corner
	line to (OPAMP.Out.x,Here.y)
	dot
	{
Z1:		impedance( up_ to (Here.x,OPAMP.Out.y) ); rlabel(,\underline{Z}_1,)
		dot
		{
			line right_ 0.3*elen_
OUT1:			dot(,,1)
		}
	}
Z2:	impedance( down_ elen_ ); llabel(,\underline{Z}_2,)
	dot
	{
		line to (IN1.x,Here.y)
IN2:		dot(,,1)
	}
	line right_ to (OUT1.x,Here.y)
OUT2:	dot(,,1)

	tikzcoordinate( IN1, IN1 )
	tikzcoordinate( IN2, IN2 )
	tikzcoordinate( NINV, NINV.w )
	tikzcoordinate( INV, INV.w )
	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )

.PE
