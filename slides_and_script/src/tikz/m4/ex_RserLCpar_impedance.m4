.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

IN1:	dot(,,1)
	line right_ 0.25*elen_
	corner
RES:	resistor( down_ 0.75*elen_,,E, ); llabel( , R, )
	dot
	{
IND:		inductor( down_ 0.75*elen_,W,); llabel( , L, )
		dot
	}
	line right_ 0.75*elen_
	corner
CAP:	capacitor( down_ 0.75*elen_ ); llabel( , C, )
	corner
	line to (IN1.x,Here.y)
IN2:	dot(,,1)

	tikzcoordinate( IN1, IN1 )
	tikzcoordinate( IN2, IN2 )

.PE
