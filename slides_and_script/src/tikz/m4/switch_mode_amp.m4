.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

# POWER STAGE:
VSS:	dot(,,1)
	"$V_{ss}$" below
	line up_ 0.2*elen_
	dot
LS:	e_fet( up_ elen_ ) with .S at Here; rlabel(,LS,)

	move to LS.G
	line left_ 0.3*elen_
	corner
VGLS:	reversed( `source', to (Here.x,LS.S.y), P,)
	corner
	line to LS.S

	move to LS.D
SN:	dot

HS:	e_fet( up_ elen_ ) with .S at Here; rlabel(,HS,)

	move to HS.G
	line left_ 0.3*elen_
	corner
VGHS:	source( to (Here.x,HS.S.y), P,)
	corner
	line to HS.S

	move to HS.D
#	line up_ 0.2*elen_
VDD:	dot(,,1)
	"$V_{dd}$" above

# 2nd ORDER LOWPASS:
	setuncover(2,-)

	move to SN
IND:	inductor( right_ elen_, L,); llabel( ,L, )
	dot
	{
CAP:		capacitor( down_ elen_ ); llabel( ,C, )
		ground()
	}
	line right_ 0.7*elen_
	corner
RES:	resistor( down_ elen_,,E, ); llabel( ,R, )
	ground()
	resetuncover

	tikzcoordinate( VGLS_s, VGLS.s )
	tikzcoordinate( VGLS_n, VGLS.n )
	tikzcoordinate( VGHS_s, VGHS.s )
	tikzcoordinate( VGHS_n, VGHS.n )
	tikzcoordinate( RES_s, RES.s )
	tikzcoordinate( RES_n, RES.n )

.PE
