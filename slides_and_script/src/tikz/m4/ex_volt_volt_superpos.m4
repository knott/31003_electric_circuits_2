.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

VS:	source( up_ elen_,V, ); llabel(,\SI{20}{\volt}\angle{\SI{90}{\degree}},)
	corner
	resistor( right_ elen_,,E, ); llabel(,\SI{10}{\ohm})
V1:	dot
	{
		inductor( down_ elen_,W, ); llabel( ,j\SI{10}{\ohm}, )
		dot
		ground()
	}
	capacitor( right_ elen_ ); llabel(,-j\SI{5}{\ohm},)
	corner
	reversed( `source', down_ elen_,V,); llabel(,\SI{20}{\volt}\angle{\SI{30}{\degree}},)
	corner
	line to VS.s
	corner

	tikzcoordinate( V1, V1 )

.PE
