.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

IN:	line right_ 0.3*elen_; "$x$" above
	line right_ 0.3*elen_
NOT1:	[ linewid = linewid*2.5; BUFFER_gate() ]
	circle radius 0.05*elen_ with .w at Here	
	line right_ 0.3*elen_; "$x_1$" above
	line right_ 0.3*elen_
NOT2:	[ linewid = linewid*2.5; BUFFER_gate() ]
	circle radius 0.05*elen_ with .w at Here
	line right_ 0.3*elen_; "$x_2$" above
	line right_ 0.3*elen_
NOT3:	[ linewid = linewid*2.5; BUFFER_gate() ]
	circle radius 0.05*elen_ with .w at Here
	line right_ 0.3*elen_
	dot
	{
OUT:		arrow right_ 0.3*elen_; "$y$" above
	}
	line up_ 0.75*elen_
	corner
	line to (IN.w.x,Here.y) \
	  then to IN.w
	corner

	"$t_d\;$" with .c at NOT1.c
	"$t_d\;$" with .c at NOT2.c
	"$t_d\;$" with .c at NOT3.c

.PE
