.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

SRC1:	source( up_ elen_ )
	corner
Z1:	impedance( right_ 1.5*elen_ ); llabel( ,\underline{Z}_1, )
	dot
	{
Z2:		impedance( down_ elen_ ); rlabel( ,\underline{Z}_2, )
		dot
	}
Z3:	impedance( right_ 2.25*elen_ ); llabel( ,\underline{Z}_3, )
	dot
	{
Z4:		impedance( down_ elen_ ); llabel( ,\underline{Z}_4, )
		dot
	}
Z5:	impedance( right_ 1.5*elen_ ); llabel( ,\underline{Z}_5, )
	corner
SRC2:	source( down_ elen_ )
	corner
	line to (Z3.x,Here.y)
	line to SRC1.s
	corner

	tikzcoordinate( SRC1_n, SRC1.n )
	tikzcoordinate( SRC1_s, SRC1.s )
	tikzcoordinate( SRC2_n, SRC2.n )
	tikzcoordinate( SRC2_s, SRC2.s )


.PE
