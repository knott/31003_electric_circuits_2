.PS
cct_init
log_init

#include(build/tikz/m4/tikz.m4)
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
dnl	`tikzcoordinate(name,location)'
define(`tikzcoordinate', `command sprintf("\draw (%g,%g) node [shape=coordinate] (`$1'){};", (`$2').x/scale, (`$2').y/scale)')

#include(build/tikz/m4/beamer.m4)
##### add uncover for animating slides with beamer
## usage:
## setuncover(start,end)
## <whatever code should be animated>
## resetuncover
##
## where `start' is first slide to show and `end' is last slide (if all, use `-')
dnl   `setuncover(start,end)'
define(`setuncover', `command sprintf("\uncover<$1$2>{")')
dnl   `resetuncover()'
define(`resetuncover', `command sprintf("}")')

#include(build/tikz/m4/preamble.m4)
scale = 2.54 # everything in cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
arrowht = 0.25
#dimen_ = 3
### define DTU colors ###
# primary colors #
define( `dtured', `0.6, 0, 0' )
define( `dtugrey', `0.6 ,0.6, 0.6' )
# secondary colors #
define( `dtuyellow', `1.00, 0.80, 0.00' )
define( `dtuorange', `1.00, 0.60, 0.00' )
define( `dtulightred', `1.00, 0.00, 0.00' )
define( `dtubrown', `0.60, 0.00, 0.00' )
define( `dtupurple', `0.80, 0.20, 0.60' )
define( `dtuviolet', `0.40, 0.00, 0.60' )
define( `dtudarkblue', `0.20, 0.40, 0.80' )
define( `dtulightblue', `0.20, 0.80, 1.00' )
define( `dtulightgreen', `0.60, 0.80, 0.20' )
define( `dtudarkgreen', `0.40, 0.80, 0.00' )
define( `dtucoolgrey', `0.59, 0.58, 0.57' )

#ifdef(`tikz_',,`include(build/tikz/m4/tikz.m4)divert(-1)')
#ifdef(`beamer_',,`include(build/tikz/m4/beamer.m4)divert(-1)')
#ifdef(`preamble_',,`include(build/tikz/m4/preamble.m4)divert(-1)')

Origin:	Here

SRC:	source( up_ elen_ )
	move to SRC.s + ( 0.5*elen_, 0)
DC:	source( up_ elen_ , V, )
	move to DC.s + ( 0.5*elen_, 0)
AC:	source( up_ elen_ , AC, )
	move to AC.s + ( 0.5*elen_, 0)
PULSE:	source( up_ elen_ , P, )
	move to PULSE.s + ( 0.5*elen_, 0)
SQUARE:	source( up_ elen_ , U, )
	move to SQUARE.s + ( 0.5*elen_, 0)
SAW:	source( up_ elen_ , R, )
	move to SAW.s + ( 0.5*elen_, 0)
TRI:	source( up_ elen_ , T, )
	move to TRI.s + ( 0.5*elen_, 0)
GRID:	source( up_ elen_ , L, )
#	move to DC.s + ( 0.5*elen_, 0)
#PV:	pvcell( up_ elen_ )

	tikzcoordinate( SRC_n, SRC.n )
	tikzcoordinate( DC_n, DC.n )
	tikzcoordinate( AC_n, AC.n )
	tikzcoordinate( PULSE_n, PULSE.n )
	tikzcoordinate( SQUARE_n, SQUARE.n )
	tikzcoordinate( SAW_n, SAW.n )
	tikzcoordinate( TRI_n, TRI.n )
	tikzcoordinate( GRID_n, GRID.n )

.PE
