.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

VS:	source( up_ elen_,V, ); llabel(\omega=\SI{377}{\radian\per\second},\sqrt{2}\;\SI{1000}{\volt}\angle{\SI{0}{\degree}},)
	b_current(\underline{I},,O,E,0.2)
	corner
	line right_ elen_
	dot
	{
		resistor( down_ elen_,,E, ); llabel(,\SI{100}{\ohm})
		dot
		ground()
	}
	line right_ elen_
	corner
	capacitor( down_ elen_ ); llabel(,\SI{10}{\micro\farad},)
	corner
	line to VS.s
	corner

.PE
