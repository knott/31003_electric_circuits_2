.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

	dot(,,1)
RES1:	resistor( down_ 1.5*elen_,,E, ); llabel( ,R, )
	setrgb(dtudarkgreen)
	b_current( i_R(t) )
	resetrgb
	dot(,,1)

	move to RES1.c + ( elen_,0 )
	"$\multimapdotbothA$" with .center at Here

	move to RES1.n+( 2*elen_, 0 )
	dot(,,1)
RES2:	resistor( down_ 1.5*elen_,,E, ); rlabel( ,R, )
	setrgb(dtudarkgreen)
	b_current( \underline{I}_R(\uls), below_, )
	resetrgb
	dot(,,1)

	tikzcoordinate( RES1_n, RES1.n )
	tikzcoordinate( RES1_s, RES1.s )
	tikzcoordinate( RES2_n, RES2.n )
	tikzcoordinate( RES2_s, RES2.s )

.PE
