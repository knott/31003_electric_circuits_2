.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

	resistor( right_ 0.75*elen_,,E, ); llabel( ,R, )
	ebox( right_ 0.75*elen,,,0); llabel( ,X, )

.PE
