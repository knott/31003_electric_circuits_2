.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

IS:	source( up_ elen_,I, )
#	setrgb(dtudarkgreen)
	b_current(\underline{I}_N,,O,E,0.2)
#	resetrgb
	corner
	line right_ elen_
	dot
	{
ZT:		impedance( down_ elen_ ); llabel(,\underline{Z}_T,)
		dot
	}
	arrowline( right_ elen_ ); llabel(,\underline{I},)
OUT1:	dot
	line right_ 0.5*elen_
	corner
ZL:	impedance( down_ elen_ ); llabel(,\underline{Z}_L,)
	corner
	line left_ 0.5*elen_
OUT2:	dot
	line to IS.s
	corner

	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )

.PE
