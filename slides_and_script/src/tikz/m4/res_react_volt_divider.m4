.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

VS:	source( up_ 1.5*elen_ )#; llabel(,\underline{V}_{src},)
#	setrgb(dtudarkgreen)
#	b_current(\underline{I}_{in},,O,E,0.2)
#	resetrgb
	corner
RSRC:	resistor( right_ 0.5*elen_,,E, ); llabel(,R_{src},)
XSRC:	ebox( right_ 0.5*elen_,,,0 ); llabel(,X_{src},)
	arrowline( right_ 0.5*elen_ ); setrgb(dtudarkgreen); llabel(,\underline{I},); resetrgb
OUT1:	dot
	line right_ 0.5*elen_
	corner
RL:	resistor( down_ 0.75*elen_,,E, ); llabel(,R_L,)
XL:	ebox( down_ 0.75*elen_,,,0 ); llabel(,X_L,)
	corner
	line left_ 0.5*elen_
OUT2:	dot
	line to VS.s
	corner

	tikzcoordinate( VS_n, VS.n )
	tikzcoordinate( VS_s, VS.s )
	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )

.PE
