.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

SRC:	source( up_ elen_ )
	corner
CAP:	capacitor( right_ elen_ ); llabel( ,C, )
	dot
	{
IND:		inductor( down_ elen_,W, ); llabel( ,L, )
		dot
	}
	line right_ 0.75*elen_
	corner
RES:	resistor( down_ elen_,,E, ); rlabel( ,R, )
	corner
	line to SRC.s
	corner

	tikzcoordinate( SRC_n, SRC.n )
	tikzcoordinate( SRC_s, SRC.s )
	tikzcoordinate( RES_n, RES.n )
	tikzcoordinate( RES_s, RES.s )


.PE
