.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

VS:	source( up_ elen_, V, ); llabel(,\SI{20}{\volt}\angle{\SI{90}{\degree}},)
	corner
	resistor( right_ elen_,,E, ); llabel(,\SI{5}{\ohm})
V1:	dot
	{
		reversed( `source', down_ elen_, I,); llabel(,\SI{3}{\ampere}\angle{\SI{0}{\degree}},)
		dot
		ground()
	}
	inductor( right_ elen_,W, ); llabel( ,j\SI{5}{\ohm}, )
	corner
	resistor( down_ elen_,,E, ); llabel(,\SI{5}{\ohm},)
	corner
	line to VS.s
	corner

	tikzcoordinate( V1, V1 )

.PE
