.PS
cct_init
log_init

### defining additional components:
divert(-1)
    `pvcell( linespec, wid, ht )'
define(`pvcell',`eleminit_(`$1')
  define(`m4ht',ifelse(`$3',,`dimen_/5',`($3)'))dnl
  define(`m4wd',ifelse(`$2',,`m4ht*2',`($2)'))dnl
  {line to rvec_(max(0,rp_len/2-m4wd/2),0)
  	{lbox(m4wd,m4ht)}
	{line from rvec_(0,-m4ht/2) to rvec_(m4wd/3,0)}
	{line from rvec_(0,m4ht/2) to rvec_(m4wd/3,0)}
	move to rvec_(m4wd,0); line to rvec_(max(0,rp_len/2-m4wd/2),0)
  }
  {[box invis ht_ m4ht wid_ m4wd] at rvec_(rp_len/2,0)}
  line to rvec_(rp_len,0) invis
')
right_
divert(0)dnl

#include(build/tikz/m4/tikz.m4)
##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
dnl	`tikzcoordinate(name,location)'
define(`tikzcoordinate', `command sprintf("\draw (%g,%g) node [shape=coordinate] (`$1'){};", (`$2').x/scale, (`$2').y/scale)')

#include(build/tikz/m4/beamer.m4)
##### add uncover for animating slides with beamer
## usage:
## setuncover(start,end)
## <whatever code should be animated>
## resetuncover
##
## where `start' is first slide to show and `end' is last slide (if all, use `-')
dnl   `setuncover(start,end)'
define(`setuncover', `command sprintf("\uncover<$1$2>{")')
dnl   `resetuncover()'
define(`resetuncover', `command sprintf("}")')

#include(build/tikz/m4/preamble.m4)
scale = 2.54 # everything in cm
elen = 2
#define(‘dimen_’,(dimen_*2))
#linethick = 2
linethick_(2.0)
arrowht = 0.25
#dimen_ = 3
### define DTU colors ###
# primary colors #
define( `dtured', `0.6, 0, 0' )
define( `dtugrey', `0.6 ,0.6, 0.6' )
# secondary colors #
define( `dtuyellow', `1.00, 0.80, 0.00' )
define( `dtuorange', `1.00, 0.60, 0.00' )
define( `dtulightred', `1.00, 0.00, 0.00' )
define( `dtubrown', `0.60, 0.00, 0.00' )
define( `dtupurple', `0.80, 0.20, 0.60' )
define( `dtuviolet', `0.40, 0.00, 0.60' )
define( `dtudarkblue', `0.20, 0.40, 0.80' )
define( `dtulightblue', `0.20, 0.80, 1.00' )
define( `dtulightgreen', `0.60, 0.80, 0.20' )
define( `dtudarkgreen', `0.40, 0.80, 0.00' )
define( `dtucoolgrey', `0.59, 0.58, 0.57' )

Origin:	Here

SRC:	source( up_ elen_ )
	corner
	line right_ 0.5*elen_
	arrowline( right_ elen_ ); llabel(,i_1(t),)
	corner
LPRIM:	inductor( down_ elen_ ,W, ); rlabel(, L_1,)
	corner
	line left_ 1.5*elen_

	move to LPRIM.s+(1.8*elen_,0)
OUT2:	dot(,,1)
	line left_ elen_
	corner
LSEC:	inductor( up_ elen_, W, ); rlabel(, L_2,)
	corner
	reversed(`arrowline', right_ elen_ ); llabel(,i_2(t),)
OUT1:	dot(,,1)

	move to LPRIM.c+(0.15*elen_,0.2*elen_)
	dot
	move to LSEC.c+(-0.15*elen_,0.2*elen_)
	dot

	tikzcoordinate( SRC_s, SRC.s )
	tikzcoordinate( SRC_n, SRC.n )
	tikzcoordinate( LPRIM_n, LPRIM.n )
	tikzcoordinate( LPRIM_c, LPRIM.c )
	tikzcoordinate( LPRIM_s, LPRIM.s )
	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )
	tikzcoordinate( LSEC_n, LSEC.n )
	tikzcoordinate( LSEC_c, LSEC.c )
	tikzcoordinate( LSEC_s, LSEC.s )

.PE
