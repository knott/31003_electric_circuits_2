.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

HP:	nport( wid 1.1*elen_ ht 0.9*elen_ "$\underline{H}_{HP}(\uls)$",1,0,1,0,3,,N )

	move to HP.W1a
#	reversed( `arrowline', left_ 0.5*elen_ ); setrgb(dtudarkgreen); rlabel( ,\underline{I}_{in}(\uls), ); resetrgb
	line left_ 0.5*elen_
IN1:	dot(,,1)

	move to HP.W1b
	line left_ 0.5*elen_
IN2:	dot(,,1)

	move to HP.e + (elen_,0)
LP:	nport( wid 1.1*elen_ ht 0.9*elen_ "$\underline{H}_{LP}(\uls)$",1,0,1,0,3,,N ) with .w at Here

	move to LP.W1a
	line left_ 0.5*elen_
	dot
	line to HP.E1a

	move to LP.W1b
	line left_ 0.5*elen_
	dot
	line to HP.E1b

	move to LP.E1a
#	arrowline( right_ 0.5*elen_ ); setrgb(dtudarkgreen); llabel( ,\underline{I}_{out}(\uls), ); resetrgb
	line right_ 0.5*elen_
OUT1:	dot(,,1)

	move to (Here.x,LP.E1b.y)
OUT2:	dot(,,1)
	line to LP.E1b

	tikzcoordinate( IN1, IN1 )
	tikzcoordinate( IN2, IN2 )
	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )

.PE
