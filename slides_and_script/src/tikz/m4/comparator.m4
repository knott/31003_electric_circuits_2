.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

	ground()
SRC:	source( up_ elen_, AC,)
	corner
	line right_ 1*elen_
OPAMP:	opamp( right_,{\scriptsize$\;\;\;\;+$},{\scriptsize$\;\;\;\;-$},1,P ) with .In1 at Here

	move to OPAMP.In2
	line left_ 0.5*elen_
	corner
	ground()

	move to OPAMP.V1
	line up_ 0.3*elen_
	dot(,,)
	"$V_{dd}$" above

	move to OPAMP.V2
	line down_ 0.3*elen_
	dot(,,)
	"$V_{ss}$" below

	move to OPAMP.Out
	line right_ 0.3*elen_

.PE
