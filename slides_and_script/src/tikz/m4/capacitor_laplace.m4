.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

	dot(,,1)
CAP1:	capacitor( down_ 1.5*elen_ ); llabel( ,C, )
	setrgb(dtudarkgreen)
	b_current( i_C(t) )
	resetrgb
	dot(,,1)

	setuncover(3,-)
	move to CAP1.c + ( 0.8*elen_,0 )
	"$\multimapdotbothA$" with .center at Here

	move to CAP1.n+( 1.5*elen_, 0 )
	dot(,,1)
PAR1:	arrowline( down_ 0.375*elen_ ); setrgb(dtudarkgreen); rlabel( ,\underline{I}_C(\uls), ); resetrgb
	dot
	{
CAP2:		capacitor( down_ 0.75*elen_ ); rlabel( ,C, )
		dot
	}
	line right_ 1.6*elen_
	corner
ISRC:	reversed( `source', down_ 0.75*elen_,I, ); rlabel( ,C v_C(0) {\scriptstyle \delta(t) }, )
	corner
	line to CAP2.s
PAR2:	line down_ 0.375*elen_
	dot(,,1)
	resetuncover

	setuncover(5,-)
	move to ISRC.c + ( 1.2*elen_,0 )
	"$\Rightarrow$" with .center at Here

	move to ISRC.n+( 2.7*elen_, 0.375*elen_ )
	dot(,,1)
CAP3:	capacitor( down_ 0.75*elen_ ); rlabel( ,C, )
VSRC:	reversed( `source', down_ 0.75*elen_,V, ); rlabel( ,\frac{v_C(0) \delta(t)}{\uls}, )
	setrgb(dtudarkgreen)
	b_current( \underline{I}_C(\uls), below_, )
	resetrgb
	dot(,,1)
	resetuncover

	tikzcoordinate( CAP1_n, CAP1.n )
	tikzcoordinate( CAP1_s, CAP1.s )
	tikzcoordinate( PAR1_n, PAR1.n )
	tikzcoordinate( PAR2_s, PAR2.s )
	tikzcoordinate( CAP3_n, CAP3.n )
	tikzcoordinate( VSRC_s, VSRC.s )

.PE
