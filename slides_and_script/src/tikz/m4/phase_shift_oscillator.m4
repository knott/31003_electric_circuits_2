.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

C1:	capacitor( right_ elen_ )
	dot
	{
R1:		resistor( down_ elen_,,E, )
		ground()
	}
C2:	capacitor( right_ elen_ )
	dot
	{
R2:		resistor( down_ elen_,,E, )
		ground()
	}
C3:	capacitor( right_ elen_ )
	dot
	{
R3:		resistor( down_ elen_,,E, )
		ground()
	}
	line right_ 0.3*elen_
	amp( right_ ); llabel(,-A,)
	line right_ 0.3*elen_
	corner
	line up_ 0.75*elen_
	corner
	line to (C1.w.x,Here.y) \
	  then to C1.w
	corner
	

.PE
