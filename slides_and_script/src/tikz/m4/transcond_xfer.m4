.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

TWOPRT:	nport( wid 1.1*elen_ ht 0.9*elen_ "$\underline{H}_V(\uls)$",1,0,1,0,3,,N )

	move to TWOPRT.W1a
#	reversed( `arrowline', left_ 0.5*elen_ ); setrgb(dtudarkgreen); rlabel( ,\underline{I}_{in}(\uls), ); resetrgb
	line left_ 0.5*elen_
	corner
VSRC:	source( down_ to (Here.x,TWOPRT.W1b.y) )
	corner
	line to TWOPRT.W1b

	move to TWOPRT.E1a
	arrowline( right_ 0.5*elen_ ); setrgb(dtudarkgreen); llabel( ,\underline{I}_{out}(\uls), ); resetrgb
#	line right_ 0.5*elen_
	corner
	line to (Here.x,TWOPRT.E1b.y)
	corner
	line to TWOPRT.E1b

	tikzcoordinate( TWOPRT_w, TWOPRT.w )
	tikzcoordinate( TWOPRT_e, TWOPRT.e )
	tikzcoordinate( VSRC_n, VSRC.n )
	tikzcoordinate( VSRC_s, VSRC.s )
#	tikzcoordinate( OUT1, OUT1 )
#	tikzcoordinate( OUT2, OUT2 )

.PE
