.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

IN1:	dot(,,1)
Z1:	impedance( right_ elen_ ); llabel( ,\underline{Z}_1, )
	dot
	{
		line down_ 0.5*elen_
		corner
INV:		arrowline( right_ 0.5*elen_ ); llabel(,\underline{I}_N,)
OPAMP:		opamp( right_,,,1.2, ) with .In1 at Here
	}
	{
Z2:		impedance( right_ to (OPAMP.Out.x,Here.y),,E,); llabel( ,\underline{Z}_2, )
		dot
		{
			line right_ 0.3*elen_
OUT1:			dot(,,1)
		}
		line to OPAMP.Out
		corner
	}

	move to OPAMP.In2
	reversed( `arrowline', left_ 0.5*elen_ ); llabel(,\underline{I}_P,)
	corner
NINV:	line down_ 0.5*elen_
	dot
	{
		line to (OUT1.x,Here.y)
OUT2:		dot(,,1)
	}
	line to (IN1.x,Here.y)
IN2:	dot(,,1)

	tikzcoordinate( IN1, IN1 )
	tikzcoordinate( IN2, IN2 )
	tikzcoordinate( INV, INV.w )
	tikzcoordinate( NINV, NINV.n )
	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )

.PE
