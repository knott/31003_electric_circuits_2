.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

GAIN:	amp( right_ ); llabel(,A,)
	line right_ 0.5*elen_
	corner
	line down_ elen_
	corner

RES:	resistor( left_ 2/3*elen_,,E,); rlabel(,R,)
CAP:	capacitor( left_ 2/3*elen_ ); rlabel(,C,)
IND:	inductor( left_ 2/3*elen_, W,); rlabel(,L,)
	dot
	{
		ground()
	}
	line to (Here.x,GAIN.w.y) \
	  then to (GAIN.w)

.PE
