.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

VS:	source( up_ elen_ )
	corner
RES:	resistor( right_ 1.5*elen_,,E, ); rlabel( ,\SI{100}{\ohm}, )
	setrgb(dtudarkgreen)
	b_current( i(t) )
	resetrgb
	corner
IND:	inductor( down_ elen_,W, ); rlabel( ,\SI{200}{\milli\henry} )
	corner
	line to VS.s
	corner

	tikzcoordinate( VS_n, VS.n )
	tikzcoordinate( VS_s, VS.s )
	tikzcoordinate( RES_w, RES.w )
	tikzcoordinate( RES_e, RES.e )
	tikzcoordinate( IND_n, IND.n )
	tikzcoordinate( IND_s, IND.s )

.PE
