.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

VIN: source(up_ 1.5*elen_, V); llabel(,V_{src})
     corner
     line right_ elen_
     dot
     {
CIN:	capacitor(to (Here.x,VIN.s.y)); rlabel(,C_{in},)
	dot
     }
     line right_ elen_
     dot
     {
RES:	resistor(down_ 0.75*elen_,E); rlabel(,R,)
	dot
ZENER:	diode(up_ 0.75*elen_ with .n at Here,Z)
	move to ZENER.s
	dot
     }
     line right_ 0.5*elen_
TRANS: e_fet( left_ elen_) with .D at Here
     move to TRANS.S
     line right_ 0.5*elen_
     dot
     {
COUT:	capacitor(to (Here.x,VIN.s.y))
	rlabel(C_{out})
	dot
     }
     line right_ elen_
     corner
LOAD: resistor(down_ 1.5*elen_,E); rlabel(,,R_{load})
     corner
     line to VIN.s
     corner

     move to RES.s
     line to (TRANS.G.x,Here.y)
     corner
     line to TRANS.G

     tikzcoordinate( LOAD_n, LOAD.n )
     tikzcoordinate( LOAD_s, LOAD.s )

.PE
