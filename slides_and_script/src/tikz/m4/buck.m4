.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

SRC:  source( up_ 2*elen_, V, )
      corner
      line right_ 1.5*elen_
#      corner

      move to SRC.s
      line right_ 1.5*elen_
DIODE:  diode( up_ elen_ )
SN:   dot
SW:   switch( up_ elen_ )
      corner

      move to SN
      line left_ 0.3*elen_
      corner
VGS:  source( up_ 0.5*elen_, P, ); llabel( , V_{GS} )
      corner
      line right_ 0.2*elen_

      move to SN
      inductor( right_ 2*elen_, L, ); llabel( , L )
      b_current( I_L )
      dot
      {
CAP:	capacitor( down_ elen_ ); rlabel( , C )
	dot
      }
      line right_ elen_
      b_current( I_{out} )
      corner
LOAD: resistor( down_ elen_, E ); llabel( , R_L )
      corner
      line to DIODE.s
      dot
      line to SRC.s
      corner

      tikzcoordinate( SRC_s, SRC.s )
      tikzcoordinate( SRC_n, SRC.n )
      tikzcoordinate( SW_n, SW.n )
      tikzcoordinate( SN, SN )
      tikzcoordinate( CAP_s, CAP.s )
      tikzcoordinate( CAP_n, CAP.n )
      tikzcoordinate( DIODE_s, DIODE.s )
      tikzcoordinate( DIODE_n, DIODE.n )
      tikzcoordinate( LOAD_s, LOAD.s )
      tikzcoordinate( LOAD_n, LOAD.n )
      tikzcoordinate( VGS_s, VGS.s )
      tikzcoordinate( VGS_n, VGS.n )

.PE
