.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

VS:	source( up_ 1.5*elen_ )#; llabel(,\underline{V}_{src},)
#	setrgb(dtudarkgreen)
#	b_current(\underline{I}_{in},,O,E,0.2)
#	resetrgb
	corner
ZSRC:	impedance( right_ 0.75*elen_ ); llabel(,\underline{Z}_{src},)
	arrowline( right_ 0.75*elen_ ); setrgb(dtudarkgreen); llabel(,\underline{I},); resetrgb
OUT1:	dot
	line right_ 0.5*elen_
	corner
ZL:	impedance( down_ 1.5*elen_ ); llabel(,\underline{Z}_L,)
	corner
	line left_ 0.5*elen_
OUT2:	dot
	line to VS.s
	corner

	tikzcoordinate( VS_n, VS.n )
	tikzcoordinate( VS_s, VS.s )
	tikzcoordinate( OUT1, OUT1 )
	tikzcoordinate( OUT2, OUT2 )

.PE
