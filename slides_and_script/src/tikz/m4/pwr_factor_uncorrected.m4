.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here

VS:	source( up_ 1.5*elen_,AC, )#; llabel(,\underline{V}_{src},)
#	setrgb(dtudarkgreen)
#	b_current(\underline{I}_{in},,O,E,0.2)
#	resetrgb
	corner
	line right_ 1.5 * elen_
	corner
LLOAD:	inductor( down_ 0.75*elen_,W, ); llabel(,L,)
RLOAD:	resistor( down_ 0.75*elen_,,E, ); llabel(,R,)
#	arrowline( right_ 0.75*elen_ ); setrgb(dtudarkgreen); llabel(,\underline{I},); resetrgb
	corner
	line to VS.s
	corner

	tikzcoordinate( VS_n, VS.n )
	tikzcoordinate( VS_s, VS.s )

.PE
