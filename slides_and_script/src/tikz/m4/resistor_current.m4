.PS
cct_init
log_init

ifdef(`preamble_',,`include(preamble.m4)')

Origin: Here                   # Position names are capitalized

RES:	resistor( right_ elen_, , E, ); llabel(,R,)
	setrgb(dtudarkgreen)
	b_current(i_R(t),,O,E,0.2)
	resetrgb

	tikzcoordinate( RES_w, RES.w )
	tikzcoordinate( RES_c, RES.c )
	tikzcoordinate( RES_e, RES.e )

.PE
