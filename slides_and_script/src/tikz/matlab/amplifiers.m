% initialize:
clear all;
close all;

% frequency:
f = logspace(-1,3,101);
omega = 2 * pi .* f;
s = i .* omega;

% constants:
f_c = 1;
f_c_buffer = 100;
tau = 1 / ( 2 * pi * f_c );
tau_buffer = 1 / ( 2 * pi * f_c_buffer );
d = 0.2;
A = 1000;
A_buffer = 10;

% transfer functions:
OPAMP = A ./ ( 1 + s .* tau );
BUFFER  = A_buffer ./ ( 1 + s .* tau_buffer );
DEN = 1 + s.* 2 .* d .* tau + s.^2 .* tau^2;
SMPA = A ./ DEN;

% results:
OPAMP_ampl_dB = 20 * log10( abs( OPAMP ) );
OPAMP_phase_deg = rad2deg( phase( OPAMP ) );
BUFFER_ampl_dB = 20 * log10( abs( BUFFER ) );
BUFFER_phase_deg = rad2deg( phase( BUFFER ) );
SMPA_ampl_dB = 20 * log10( abs( SMPA ) );
SMPA_phase_deg = rad2deg( phase( SMPA ) );

% preview:
subplot(2,1,1);
semilogx( f, OPAMP_ampl_dB, f, SMPA_ampl_dB, f, BUFFER_ampl_dB );
subplot(2,1,2);
semilogx( f, OPAMP_phase_deg, f, SMPA_phase_deg, f, BUFFER_phase_deg );

% export to file:
csvwrite( 'opamp.csv', [ f', OPAMP_ampl_dB', OPAMP_phase_deg' ] );
csvwrite( 'buffer.csv', [ f', BUFFER_ampl_dB', BUFFER_phase_deg' ] );
csvwrite( 'switch_mode_amp.csv', [ f', SMPA_ampl_dB', SMPA_phase_deg' ] );