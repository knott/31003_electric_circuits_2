% initialize:
clear all;
close all;

% frequency:
f = logspace(-2,2,1001);
omega = 2 * pi .* f;
s = i .* omega;

% constants:
A_0 = 8;
%A_0 = 1;
f_p = 1;
T_p = 1 / ( 2 * pi * f_p );
f_z = 100;
tau_z = 1 / ( 2 * pi * f_z );
%d = [2 1 0.5 0.2]';% corresponds to Q = [0.5 1 2 5]
d = 1;%

% transfer functions:
DEN = ( 1 + s.* 2 .* d .* T_p + s.^2 .* T_p^2 ) .* ( 1 + s .* T_p);
%OSC = A_0 .* ( 1 + s.* tau_z )./ DEN;
%OSC = 1 ./ DEN;
OSC = A_0 ./ DEN;

% results:
OSC_ampl_dB = 20 * log10( abs( OSC ) );
n = 0;
OSC_phase_deg = zeros( size(d,1), length(f) );
for n = 1:size(d,1)
    OSC_phase_deg(n,:) = rad2deg( phase( OSC(n,:) ) );
end
OSC_real = real( OSC );
OSC_imag = imag( OSC );

% preview lowpass:
figure;
subplot(2,1,1);
semilogx( f, OSC_ampl_dB );
%semilogx( f, OSC_ampl_dB(1,:), f, OSC_ampl_dB(2,:), f, OSC_ampl_dB(3,:), f, OSC_ampl_dB(4,:) );
grid on;
subplot(2,1,2);
semilogx( f, OSC_phase_deg );
%semilogx( f, OSC_phase_deg(1,:), f, OSC_phase_deg(2,:), f, OSC_phase_deg(3,:), f, OSC_phase_deg(4,:) );
grid on;

figure;
plot( OSC_real, OSC_imag );
%plot( OSC_real(1,:), OSC_imag(1,:), OSC_real(2,:), OSC_imag(2,:), OSC_real(3,:), OSC_imag(3,:), OSC_real(4,:), OSC_imag(4,:) );
grid on;

% export to file:
csvwrite( 'oscillator.csv', [ f', OSC_ampl_dB', OSC_phase_deg', OSC_real', OSC_imag' ] );