% initialize:
clear all;
close all;

% frequency:
samples = 100;
t = 0:2*pi/samples:2*pi;

% constants:
Vin_pk1 = 0.4;
Vin_pk2 = 0.5;
Vin1 = Vin_pk1 .* sin( t );
Vin2 = Vin_pk2 .* sin( t );

% Nonlinear Gain:
gain = 2;
dist_points = 5;% odd number required
gain_crossover = 0.2;
crossover = ones( 1,length( t ) ) .* gain;
crossover( 1:( dist_points - 1 ) / 2 + 1 ) = ones( 1, ( dist_points - 1 ) / 2 + 1 ) .* gain_crossover;
crossover( ( samples-( dist_points - 1 ) / 2 + 1 ):samples ) = ones( 1, ( dist_points - 1 ) / 2 ) .* gain_crossover;
crossover( ( samples/2 - ( dist_points - 1 ) / 2 ) + 1:( samples/2 + ( dist_points - 1 ) / 2 ) + 1)= ones( 1,dist_points ) .* gain_crossover;

% output:
Vout_crossover = Vin1 .* crossover;
Vout_crossover_smooth = smooth(Vout_crossover,3)';
Vout_clipping = min( Vin2 .* gain, 0.8);
Vout_clipping = max( Vout_clipping, -0.8);

% % FFT:
% Vin1_fft = fft( Vin1 );
% Vout_crossover_fft = fft( Vout_crossover );
% Vout_clipping_fft = fft( Vout_clipping );
% Vin1_abs = abs( Vin1_fft ) .* 2 ./ samples;
% Vout_crossover_abs = abs( Vout_crossover_fft ) .* 2 ./ samples;
% Vout_clipping_abs = abs( Vout_clipping_fft ) .* 2 ./ samples;

% preview:
plot( t, Vout_crossover, t, Vout_crossover_smooth, t, Vout_clipping );

% figure
% subplot(2,1,1);
% plot( Vin1_abs );
% hold on;
% plot( Vout_crossover_abs  );
% plot( Vout_clipping_abs  );
% subplot(2,1,2);
% plot( phase( rad2deg( Vin1_fft ) ) );
% hold on;
% plot( phase( rad2deg( Vout_crossover_fft ) ) );

% export to file:
csvwrite( 'crossover.csv', [ t', Vout_crossover' ] );
csvwrite( 'crossover_smooth.csv', [ t', Vout_crossover_smooth' ] );
csvwrite( 'clipping.csv', [ t', Vout_clipping' ] );