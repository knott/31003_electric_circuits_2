% initialize:
clear all;
close all;

% frequency:
f = logspace(-2,2,101);
omega = 2 * pi .* f;
s = i .* omega;

% constants:
f_c = 1;
f_c1 = 0.1;
f_c2 = 10;
tau = 1 / ( 2 * pi * f_c );% where tau = R*C, so 1/100 puts the pole/zero at 100 Hertz
tau1 = 1 / ( 2 * pi * f_c1 );% first pole/zero for the bandpass / bandstop
tau2 = 1 / ( 2 * pi * f_c2 );% first pole/zero for the bandpass / bandstop

% transfer functions:
LP = 1 ./ ( 1 + s .* tau );
HP = s .* tau ./ ( 1 + s .* tau );
BP = s .* tau1 ./ ( ( 1 + s .* tau1 ) .* ( 1 + s .* tau2 ) );
BS = ( s .* tau2 .* ( 1 + s .* tau1 ) + ( 1 + s .* tau2 ) ) ./ ( ( 1 + s .* tau1 ) .* ( 1 + s .* tau2 ) );

% results:
LP_ampl_dB = 20 * log10( abs( LP ) );
LP_phase_deg = rad2deg( phase( LP ) );
HP_ampl_dB = 20 * log10( abs( HP ) );
HP_phase_deg = rad2deg( phase( HP ) );
BP_ampl_dB = 20 * log10( abs( BP ) );
BP_phase_deg = rad2deg( phase( BP ) );
BS_ampl_dB = 20 * log10( abs( BS ) );
BS_phase_deg = rad2deg( phase( BS ) );
% LP_ampl_asympt = zeros( 1, length( f ) );
% LP_phase_asympt = zeros( 1, length( f ) );
% HP_ampl_asympt = zeros( 1, length( f ) );
% HP_phase_asympt = zeros( 1, length( f ) );
% BP_ampl_asympt = zeros( 1, length( f ) );
% BP_phase_asympt = zeros( 1, length( f ) );
% BS_ampl_asympt = zeros( 1, length( f ) );
% BS_phase_asympt = zeros( 1, length( f ) );
% n = 1;
% while n <= length( f )
%     if f( n ) < f_c1
%         LP_phase_asympt( n ) = 90;
%         BP_ampl_asympt( n ) = 20 * log10( abs( s( n ) ) ) - 20;
%     end
%     if f( n ) < f_c
%         HP_ampl_asympt( n ) = 20 * log10( abs( s( n ) ) ) - 40; 
%     end
%     if f( n ) > f_c
%         LP_ampl_asympt( n ) = 20 * log10( abs( 1 / s( n ) ) ) + 40;
%     end
%     if ( f( n ) > f_c1 ) & ( f( n ) < f_c )
%         BS_ampl_asympt = 20 * log10( abs( 1 / s( n ) ) ) + 20;
%     end
%     if ( f( n ) > f_c ) & ( f( n ) < f_c2 )
%         BS_ampl_asympt = 20 * log10( abs( s( n ) ) ) - 40;
%     end
% %    if ( f( n ) > f_c1 ) & ( f( n ) < f_c2 )
% %        LP_phase_asympt( n ) = -log10( f( n ) - f_c1 );
% %        LP_phase_asympt( n ) = -log10( f( n ) - f_c1 ) + 90;
% %    end
%     if f( n ) > f_c2
%         BP_ampl_asympt( n ) = 20 * log10( abs( 1 / s( n ) ) ) + 60;
%     end
%     n = n+1;
% end


% preview:
subplot(2,1,1);
semilogx( f, LP_ampl_dB, f, HP_ampl_dB, f, BP_ampl_dB, f, BS_ampl_dB );
subplot(2,1,2);
semilogx( f, LP_phase_deg, f, HP_phase_deg, f, BP_phase_deg, f, BS_phase_deg );
% figure;
% subplot(2,1,1);
% semilogx( f, LP_ampl_asympt, f, HP_ampl_asympt, f, BP_ampl_asympt, f, BS_ampl_asympt );
% subplot(2,1,2);
% semilogx( f, LP_phase_deg, f, HP_phase_deg, f, BP_phase_deg, f, BS_phase_deg );

% export to file:
csvwrite( 'lowpass_1st.csv', [ f', LP_ampl_dB', LP_phase_deg' ] );
csvwrite( 'highpass_1st.csv', [ f', HP_ampl_dB', HP_phase_deg' ] );
csvwrite( 'bandpass_1st.csv', [ f', BP_ampl_dB', BP_phase_deg' ] );
csvwrite( 'bandstop_1st.csv', [ f', BS_ampl_dB', BS_phase_deg' ] );