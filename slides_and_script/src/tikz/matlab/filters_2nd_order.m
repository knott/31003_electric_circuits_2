% initialize:
clear all;
close all;

% frequency:
f = logspace(-2,2,1001);
omega = 2 * pi .* f;
s = i .* omega;

% constants:
f_c = 1;
T_0 = 1 / ( 2 * pi * f_c );
d = [2 1 0.5 0.2]';% corresponds to Q = [0.5 1 2 5]

% transfer functions:
DEN = 1 + s.* 2 .* d .* T_0 + s.^2 .* T_0^2;
LP = 1 ./ DEN;
HP = s.^2 .* T_0^2 ./ DEN;
BP = s.* 2 .* d .* T_0 ./ DEN;
BS = ( 1 + s.^2 .* T_0^2 ) ./ DEN;

% results:
LP_ampl_dB = 20 * log10( abs( LP ) );
n = 0;
LP_phase_deg = zeros( size(d,1), length(f) );
for n = 1:size(d,1)
    LP_phase_deg(n,:) = rad2deg( phase( LP(n,:) ) );
end

HP_ampl_dB = 20 * log10( abs( HP ) );
n = 0;
HP_phase_deg = zeros( size(d,1), length(f) );
for n = 1:size(d,1)
    HP_phase_deg(n,:) = rad2deg( phase( HP(n,:) ) );
end

BP_ampl_dB = 20 * log10( abs( BP ) );
n = 0;
BP_phase_deg = zeros( size(d,1), length(f) );
for n = 1:size(d,1)
    BP_phase_deg(n,:) = rad2deg( phase( BP(n,:) ) );
end

BS_ampl_dB = 20 * log10( abs( BS ) );
n = 0;
BS_phase_deg = zeros( size(d,1), length(f) );
for n = 1:size(d,1)
    BS_phase_deg(n,:) = rad2deg( phase( BS(n,:) ) );
end

% preview lowpass:
figure;
subplot(2,1,1);
semilogx( f, LP_ampl_dB(1,:), f, LP_ampl_dB(2,:), f, LP_ampl_dB(3,:), f, LP_ampl_dB(4,:) );
subplot(2,1,2);
semilogx( f, LP_phase_deg(1,:), f, LP_phase_deg(2,:), f, LP_phase_deg(3,:), f, LP_phase_deg(4,:) );

figure;
subplot(2,1,1);
semilogx( f, HP_ampl_dB(1,:), f, HP_ampl_dB(2,:), f, HP_ampl_dB(3,:), f, HP_ampl_dB(4,:) );
subplot(2,1,2);
semilogx( f, HP_phase_deg(1,:), f, HP_phase_deg(2,:), f, HP_phase_deg(3,:), f, HP_phase_deg(4,:) );

figure;
subplot(2,1,1);
semilogx( f, BP_ampl_dB(1,:), f, BP_ampl_dB(2,:), f, BP_ampl_dB(3,:), f, BP_ampl_dB(4,:) );
subplot(2,1,2);
semilogx( f, BP_phase_deg(1,:), f, BP_phase_deg(2,:), f, BP_phase_deg(3,:), f, BP_phase_deg(4,:) );

figure;
subplot(2,1,1);
semilogx( f, BS_ampl_dB(1,:), f, BS_ampl_dB(2,:), f, BS_ampl_dB(3,:), f, BS_ampl_dB(4,:) );
subplot(2,1,2);
semilogx( f, BS_phase_deg(1,:), f, BS_phase_deg(2,:), f, BS_phase_deg(3,:), f, BS_phase_deg(4,:) );

% export to file:
csvwrite( 'lowpass_2nd.csv', [ f', LP_ampl_dB(1,:)', LP_phase_deg(1,:)', LP_ampl_dB(2,:)', LP_phase_deg(2,:)', LP_ampl_dB(3,:)', LP_phase_deg(3,:)', LP_ampl_dB(4,:)', LP_phase_deg(4,:)' ] );
csvwrite( 'highpass_2nd.csv', [ f', HP_ampl_dB(1,:)', HP_phase_deg(1,:)', HP_ampl_dB(2,:)', HP_phase_deg(2,:)', HP_ampl_dB(3,:)', HP_phase_deg(3,:)', HP_ampl_dB(4,:)', HP_phase_deg(4,:)' ] );
csvwrite( 'bandpass_2nd.csv', [ f', BP_ampl_dB(1,:)', BP_phase_deg(1,:)', BP_ampl_dB(2,:)', BP_phase_deg(2,:)', BP_ampl_dB(3,:)', BP_phase_deg(3,:)', BP_ampl_dB(4,:)', BP_phase_deg(4,:)' ] );
csvwrite( 'bandstop_2nd.csv', [ f', BS_ampl_dB(1,:)', BS_phase_deg(1,:)', BS_ampl_dB(2,:)', BS_phase_deg(2,:)', BS_ampl_dB(3,:)', BS_phase_deg(3,:)', BS_ampl_dB(4,:)', BS_phase_deg(4,:)' ] );