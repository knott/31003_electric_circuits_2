% initialize:
clear all;
close all;

% constants:
vC0 = 2;
Vpk = 5;
t1 = 30e-6;
R1 = 1e3;
R2 = 100;
R3 = 10e3;
C = 22e-9;

% time:
t = 0:1e-6:150e-6;

% results:
vC1 = vC0 .* exp( -t ./ (R1*C) ) + ...
    ( t<t1 )  .* ( Vpk/t1 .* t - Vpk/t1 * R1*C* ( 1 - exp( -t ./ (R1*C) ) ) ) + ...
    ( t>=t1 ) .* ( Vpk         - Vpk/t1 * R1*C* ( exp( - (t - t1) ./ (R1*C) ) - exp( -t ./ (R1*C) ) ) );

vC2 = vC0 .* exp( -t ./ (R2*C) ) + ...
    ( t<t1 )  .* ( Vpk/t1 .* t - Vpk/t1 * R2*C* ( 1 - exp( -t ./ (R2*C) ) ) ) + ...
    ( t>=t1 ) .* ( Vpk         - Vpk/t1 * R2*C* ( exp( - (t - t1) ./ (R2*C) ) - exp( -t ./ (R2*C) ) ) );

vC3 = vC0 .* exp( -t ./ (R3*C) ) + ...
    ( t<t1 )  .* ( Vpk/t1 .* t - Vpk/t1 * R3*C* ( 1 - exp( -t ./ (R3*C) ) ) ) + ...
    ( t>=t1 ) .* ( Vpk         - Vpk/t1 * R3*C* ( exp( - (t - t1) ./ (R3*C) ) - exp( -t ./ (R3*C) ) ) );


% preview:
hold on;
plot( t, vC1 );
plot( t, vC2 );
plot( t, vC3 );

% export to file:
csvwrite( 'vC1.csv', [ t', vC1' ] );
csvwrite( 'vC2.csv', [ t', vC2' ] );
csvwrite( 'vC3.csv', [ t', vC3' ] );