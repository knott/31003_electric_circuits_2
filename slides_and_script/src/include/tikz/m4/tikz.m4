divert(-1)
  tikz.m4

* local defined macros, copyright by Arnold Knott    *
* This file defines additional circuit macros        *
* I hope that the contents of this repository/file   *
* is of any use for you. So feel free to use it,     *
* copy it and redistribute it. Whatever you do       *
* alwyas leave this notice. There is no warramty     *
* WHATSOEVER.  Not even any implied warranty for     *
* MERCANTABILITY or FITNESS FOR A PARTICULAR PURPOSE *

define(`tikz_')

##### allowing nodes to be refered by tikz later
### from personal e-mail correspondance with D. Aplevich in 2017:
dnl	`tikznode(name,location,string)'
define(`tikznode', `command sprintf("\draw (%g,%g) node [inner sep=0pt, minimum size=0pt] (`$1'){`$3'};", (`$2').x/scale, (`$2').y/scale)')
dnl	`tikzcoordinate(name,location)'
define(`tikzcoordinate', `command sprintf("\draw (%g,%g) node [shape=coordinate] (`$1'){};", (`$2').x/scale, (`$2').y/scale)')

right_
divert(0)dnl
