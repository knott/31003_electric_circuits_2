# Course material for 31003 Electric Circuits 2
This repository contains

## [Slides](slides_and_script/output/presentation.pdf)
## [Script](slides_and_script/output/script.pdf)
<!-- [Extra slides to Active Filters](slides_and_script/output/active_second_order_filters.pdf)

[Exercises to Filters](slides_and_script/output/filter_exercises.pdf)

[Guest lecture Nenad: AC machines, synchronous generators and induction machines](guest_lectures/SM_and_IM.pdf)

[Guest lecture Ivan: Integrated Circuits - A Technology that changed the worls](guest_lectures/integrated_circuits.pdf)

[Guest lecture Finn: The Power of Electrical Circuit Models in Acoustics (pptx)](guest_lectures/Electrical_circuit_modelling_in_acoustics.pptx)

[Guest lecture Finn: The Power of Electrical Circuit Models in Acoustics (pdf)](guest_lectures/Electrical_circuit_modelling_in_acoustics.pdf)

[Guest lecture Vitaly: Wireless Technologies](guest_lectures/wireless_technologies.pdf)

[Guest lecture Hans Henrik: Reguleringsteknik](guest_lectures/reguleringsteknik.pdf)

[Electric Circuits in Ventilators](extra_material/electric_circuits_in_ventilators.pdf)

## [Solutions to Exercises](exercise_solutions/)

# [- CHANGES TO COURSE DUE TO CORONAVIRUS -]
[Course plan adjustment](CORONA_CHANGES.pdf) -->

---

Further material:

and the [LaTeX sources](slides_and_script/src/) for the slides and the script.

---

Please feel free to [create an issue](https://gitlab.gbar.dtu.dk/knott/31003_electric_circuits_2/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) for any kind of feedback (you can do this private or public).


# [LICENSE](LICENSE)